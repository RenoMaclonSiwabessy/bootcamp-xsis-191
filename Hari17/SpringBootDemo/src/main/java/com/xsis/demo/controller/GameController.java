package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.GameModel;
import com.xsis.demo.repository.GameRepo;

@Controller
public class GameController {
	//Membuat auto instance dari repository
	@Autowired
	private GameRepo repo;
	
	//Request yang ada di url localhost:port/game/index
	@RequestMapping("/game/index")
	public String index(Model model) {
		//Membuat object list game, kemudian diisi dari objecrt repo dengan method findAll
		List<GameModel> data = repo.findAll();
		
		//Mengirim variable listDataGame, valuenye diisi dari object data
		model.addAttribute("listDataGame", data);
		
		//Menampilkan view /src/main/resources/templates
		return "game/index";
	}
	
	//Request yang ada di url localhost:port/game/add
	@RequestMapping("/game/add")
	public String add() {
		//Menampilkan view /src/main/resources/templates
		return "game/add";
	}
	
	//Request yang ada di url localhost:port/game/save
	//Memiliki method POST
	@RequestMapping(value="/game/save", method = RequestMethod.POST)
	public String save(@ModelAttribute GameModel item) {
		//Simpan ke database
		repo.save(item);
		
		//Redirect : akan diteruskan ke halaman index
		return "redirect:/game/index";
	}
	
	//Request edit data
	@RequestMapping(value="/game/edit/{id}")
	public String edit(Model model, @PathVariable(name="id")Integer id) {
		GameModel item = repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "game/edit";
	}
	
	//delete
	@RequestMapping(value = "/game/delete/{id}")
	public String hapus(@PathVariable(name="id")Integer id) {
		//Mengambil data dari database dengan parameter id
		GameModel item = repo.findById(id).orElse(null);
		//Remove from database
		if(item != null) {
			repo.delete(item);
		}
		
		return "redirect:/game/index";
	}
}
