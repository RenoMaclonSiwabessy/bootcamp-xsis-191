package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.GameModel;

@Repository
public interface GameRepo extends JpaRepository<GameModel, Integer> {

}
