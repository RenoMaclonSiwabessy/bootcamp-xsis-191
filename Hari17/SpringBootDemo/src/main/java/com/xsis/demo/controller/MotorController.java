package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.MotorModel;

@Controller
public class MotorController {
	@RequestMapping("/motor/index")
	public String index() {
		return "motor/index";
	}
	
	@RequestMapping("/motor/add")
	public String add() {
		return "motor/add";
	}
	
	@RequestMapping(value="/motor/save", method = RequestMethod.POST)
	public String save(@ModelAttribute MotorModel item, Model model) {
		model.addAttribute("data", item);
		return "motor/save";
	}
}
