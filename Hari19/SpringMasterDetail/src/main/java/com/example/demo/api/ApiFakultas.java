package com.example.demo.api;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.Repository.FakultasRepo;
import com.example.demo.model.FakultasModel;

@Controller
public class ApiFakultas {
	@Autowired
	private FakultasRepo fkulrepo;
	
	private Log log= LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/api/fakultas/", method = RequestMethod.GET)
	public ResponseEntity<List<FakultasModel>> list(){
		ResponseEntity<List<FakultasModel>> hasil = null;
		
		try {
			List<FakultasModel> list = fkulrepo.findAll();
			hasil = new ResponseEntity<List<FakultasModel>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil = new ResponseEntity<List<FakultasModel>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}return hasil;
	}
	
	@RequestMapping(value = "/api/fakultas/", method = RequestMethod.POST)
	public ResponseEntity<FakultasModel> save(@RequestBody FakultasModel item){
		ResponseEntity<FakultasModel> hasil = null;
		
		try {
			fkulrepo.save(item);
			hasil = new ResponseEntity<FakultasModel>(item, HttpStatus.CREATED);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil = new ResponseEntity<FakultasModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}return hasil;
	}
	
	@RequestMapping(value ="/api/fakultas/{del}", method = RequestMethod.DELETE)
	public ResponseEntity<FakultasModel> delete(@PathVariable(name ="del") Integer id){
		ResponseEntity<FakultasModel> hasil = null;
		
		try {
			FakultasModel item = fkulrepo.findById(id).orElse(null);
			fkulrepo.delete(item);
			hasil = new ResponseEntity<FakultasModel>(item, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(),e);
			hasil = new ResponseEntity<FakultasModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}return hasil;
	}
}
