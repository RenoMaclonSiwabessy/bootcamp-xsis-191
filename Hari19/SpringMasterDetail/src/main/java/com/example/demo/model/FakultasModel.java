package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "fakultas")
public class FakultasModel {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "fakultas_seq")
	@TableGenerator(name = "fakultas_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private Integer id;
	
	@Column(name = "kd_fakultas", nullable = false, length=10)
	private String kdFakultas;
	
	@Column(name = "nm_fakultas", nullable = false, length=150)
	private String nmFakultas;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "fakultas", cascade = CascadeType.ALL)
	private List<JurusanModel> listJurusan = new ArrayList<JurusanModel>();
	
	@Column(name = "is_delete")
	private boolean isDelete;
	
	public boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKdFakultas() {
		return kdFakultas;
	}

	public void setKdFakultas(String kdFakultas) {
		this.kdFakultas = kdFakultas;
	}

	public String getNmFakultas() {
		return nmFakultas;
	}

	public void setNmFakultas(String nmFakultas) {
		this.nmFakultas = nmFakultas;
	}

	public List<JurusanModel> getListJurusan() {
		return listJurusan;
	}

	public void setListJurusan(List<JurusanModel> listJurusan) {
		this.listJurusan = listJurusan;
	}

	
	
	
}
