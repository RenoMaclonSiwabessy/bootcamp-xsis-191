package com.xsis.api.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.BiodataModel;
import com.xsis.api.repository.BiodataRepo;

@Controller
public class ApiBiodataController {
	@Autowired
	private BiodataRepo repo;
	
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/biodata/", method=RequestMethod.GET)
	public ResponseEntity<List<BiodataModel>> list(){
		ResponseEntity<List<BiodataModel>> hasil = null;
		try {
			List<BiodataModel> list = repo.findAll();
			hasil = new ResponseEntity<List<BiodataModel>>(list, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}
	
	@RequestMapping(value="/api/biodata/", method = RequestMethod.POST)
	public ResponseEntity<BiodataModel> create(@RequestBody BiodataModel item){
		ResponseEntity<BiodataModel> hasil = null;
		try {
			repo.save(item);
			hasil = new ResponseEntity<BiodataModel>(item,HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<BiodataModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
		
	}
	
	//Delete
	@RequestMapping(value="/api/delete/{id}")
	public ResponseEntity<BiodataModel> hapus(@PathVariable(name="id")Integer id) {
		ResponseEntity<BiodataModel> hasil = null;
		try {
			BiodataModel item = repo.findById(id).orElse(null);
			repo.delete(item);
			hasil = new ResponseEntity<BiodataModel>(item, HttpStatus.OK); 
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<BiodataModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return hasil;
	}
	
}
