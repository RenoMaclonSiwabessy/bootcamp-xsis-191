package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.api.model.JurusanModel;

@Repository
public interface JurusanRepo extends JpaRepository<JurusanModel, Integer> {

}
