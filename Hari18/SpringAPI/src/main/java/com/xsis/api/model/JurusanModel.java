package com.xsis.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "jurusan")
public class JurusanModel {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "jurusan_seq")
	@TableGenerator(name = "jurusan_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private int id;
	
	@Column(name = "kd_jurusan", nullable = false)
	private String kdJurusan;
	
	@Column(name = "nm_jurusan", nullable = false)
	private String nmJurusan;
	
	@Column(name = "fakultas_id", nullable = false)
	private String fakultasId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getKdJurusan() {
		return kdJurusan;
	}
	
	public void setKdJurusan(String kdJurusan) {
		this.kdJurusan = kdJurusan;
	}

	public String getNmJurusan() {
		return nmJurusan;
	}

	public void setNmJurusan(String nmJurusan) {
		this.nmJurusan = nmJurusan;
	}

	public String getFakultasId() {
		return fakultasId;
	}

	public void setFakultasId(String fakultasId) {
		this.fakultasId = fakultasId;
	}

}
