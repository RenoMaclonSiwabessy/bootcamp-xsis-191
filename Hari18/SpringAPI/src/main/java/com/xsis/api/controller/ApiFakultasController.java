package com.xsis.api.controller;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.FakultasModel;
import com.xsis.api.repository.FakultasRepo;

@Controller
public class ApiFakultasController {
	@Autowired
	private FakultasRepo repo;
	
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/fakultas/", method=RequestMethod.GET)
	public ResponseEntity<List<FakultasModel>> list(){
		ResponseEntity<List<FakultasModel>> hasil = null;
		try {
			List<FakultasModel> list = repo.findAll();
			hasil = new ResponseEntity<List<FakultasModel>>(list, HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}
	
	@RequestMapping(value="/api/fakultas/", method = RequestMethod.POST)
	public ResponseEntity<FakultasModel>create(@RequestBody FakultasModel item){
		ResponseEntity<FakultasModel> hasil = null;
		try {
			repo.save(item);
			hasil = new ResponseEntity<FakultasModel>(item, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<FakultasModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}
	
	//Delete
	@RequestMapping(value="/api/fakultas/{abc}", method = RequestMethod.DELETE)
	public ResponseEntity<FakultasModel> hapus(@PathVariable(name="abc")Integer id){
		ResponseEntity<FakultasModel> hasil = null;
		try {
			FakultasModel item = repo.findById(id).orElse(null);
			repo.delete(item);
			hasil = new ResponseEntity<FakultasModel>(item,HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e.getMessage(), e);
			hasil = new ResponseEntity<FakultasModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return hasil;
	}

}
