package hackerrank.implementation;

import java.util.*;

public class CompareTheTriplets {
	

static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        
        List<Integer> result = new ArrayList();
        
        result.add(0);
        result.add(0);

        int nAlice = 0;
        int nBob = 0;

        for(int i = 0; i < a.size();i++){
            if(a.get(i) > b.get(i)){
                nAlice++;
                result.set(0, nAlice);
            }
            if(a.get(i) < b.get(i)){
                nBob++;
                result.set(1, nBob);
        }
        }
       return result;

    }

    public static void main(String[] args){
    	List<Integer> a = new ArrayList<Integer>();
    	a.add(17);
    	a.add(28);
    	a.add(30);
    	
    	List<Integer> b = new ArrayList<Integer>();
    	b.add(99);
    	b.add(16);
    	b.add(20);
    	
    	for(Integer item : compareTriplets(a, b)) {
    		System.out.print(item + "\t");
    	}
        compareTriplets(a,b);
        
        System.out.println("\n");
        ulang1();
        System.out.println("\n");
        ulang2();
        System.out.println("\n");
        ulang3();
        System.out.println("\n");
        ulang4();
        System.out.println("\n");
        ulang5();
    }
    
    public static void ulang1() {

    	List<Integer> a1 = new ArrayList<Integer>();
    	a1.add(17);
    	a1.add(28);
    	a1.add(30);
    	
    	List<Integer> b1 = new ArrayList<Integer>();
    	b1.add(99);
    	b1.add(16);
    	b1.add(20);
    	
    	for(Integer item1 : compareTriplets(a1, b1)) {
    		System.out.print(item1 + "\t");
    	}
        compareTriplets(a1,b1);
    }
    
    public static void ulang2() {

    	List<Integer> a2 = new ArrayList<Integer>();
    	a2.add(17);
    	a2.add(28);
    	a2.add(30);
    	
    	List<Integer> b2 = new ArrayList<Integer>();
    	b2.add(99);
    	b2.add(16);
    	b2.add(20);
    	
    	for(Integer item2 : compareTriplets(a2, b2)) {
    		System.out.print(item2 + "\t");
    	}
        compareTriplets(a2,b2);
    }
    
    public static void ulang3() {

    	List<Integer> a3 = new ArrayList<Integer>();
    	a3.add(17);
    	a3.add(28);
    	a3.add(30);
    	
    	List<Integer> b3 = new ArrayList<Integer>();
    	b3.add(99);
    	b3.add(16);
    	b3.add(20);
    	
    	for(Integer item3 : compareTriplets(a3, b3)) {
    		System.out.print(item3 + "\t");
    	}
        compareTriplets(a3,b3);
    }
    
    public static void ulang4() {

    	List<Integer> a4 = new ArrayList<Integer>();
    	a4.add(17);
    	a4.add(28);
    	a4.add(30);
    	
    	List<Integer> b4 = new ArrayList<Integer>();
    	b4.add(99);
    	b4.add(16);
    	b4.add(20);
    	
    	for(Integer item4 : compareTriplets(a4, b4)) {
    		System.out.print(item4 + "\t");
    	}
        compareTriplets(a4,b4);
    }
    
    public static void ulang5() {

    	List<Integer> a5 = new ArrayList<Integer>();
    	a5.add(17);
    	a5.add(28);
    	a5.add(30);
    	
    	List<Integer> b5 = new ArrayList<Integer>();
    	b5.add(99);
    	b5.add(16);
    	b5.add(20);
    	
    	for(Integer item5 : compareTriplets(a5, b5)) {
    		System.out.print(item5+ "\t");
    	}
        compareTriplets(a5,b5);
    }

}
