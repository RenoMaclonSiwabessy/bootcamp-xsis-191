package hackerrank.implementation;

import java.util.Scanner;

public class AppleAndOrange {
	
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        int capples = 0;
        for(int x = 0; x < apples.length; x++){
            int jarak = a + apples[x];
            if(jarak >= s && jarak <= t){
            capples++;
            }

        }
        int coranges = 0;
        for(int y = 0; y < oranges.length; y++){
            int jarak = b + oranges[y];
            if(jarak >= s && jarak <= t){
            coranges++;
            }
        }
        System.out.println(capples);
        System.out.println(coranges);


    }

    static Scanner scn,scn1,scn2,scn3,scn4,scn5; 
    
    public static void main(String[] args) {
    	scn = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s = scn.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t = scn.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a = scn.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b = scn.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m = scn.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n = scn.nextInt();
    	
    	int [] apel = new int[m];
    	int [] jeruk = new int[n];
    	
    	for (int i = 0; i < apel.length; i++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i+1) + " : ");
    		int bapel = scn.nextInt();
    		apel[i] = bapel;
    	}
    	
    	for (int j = 0; j < jeruk.length; j++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j+1) + " : ");
    		int bjeruk = scn.nextInt();
    		jeruk[j] = bjeruk;
    	}
    	
    	countApplesAndOranges(s, t, a, b, apel, jeruk);
    	
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
    
    public static void ulang1() {

    	scn1 = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s1 = scn1.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t1 = scn1.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a1 = scn1.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b1 = scn1.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m1 = scn1.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n1 = scn1.nextInt();
    	
    	int [] apel1 = new int[m1];
    	int [] jeruk1 = new int[n1];
    	
    	for (int i1 = 0; i1 < apel1.length; i1++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i1+1) + " : ");
    		int bapel1 = scn.nextInt();
    		apel1[i1] = bapel1;
    	}
    	
    	for (int j1 = 0; j1 < jeruk1.length; j1++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j1+1) + " : ");
    		int bjeruk1 = scn1.nextInt();
    		jeruk1[j1] = bjeruk1;
    	}
    	
    	countApplesAndOranges(s1, t1, a1, b1, apel1, jeruk1);
    }
    
    public static void ulang2() {

    	scn2 = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s2 = scn2.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t2 = scn2.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a2 = scn2.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b2 = scn2.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m2 = scn2.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n2 = scn2.nextInt();
    	
    	int [] apel2 = new int[m2];
    	int [] jeruk2 = new int[n2];
    	
    	for (int i2 = 0; i2 < apel2.length; i2++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i2+1) + " : ");
    		int bapel2 = scn2.nextInt();
    		apel2[i2] = bapel2;
    	}
    	
    	for (int j2 = 0; j2 < jeruk2.length; j2++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j2+1) + " : ");
    		int bjeruk2 = scn2.nextInt();
    		jeruk2[j2] = bjeruk2;
    	}
    	
    	countApplesAndOranges(s2, t2, a2, b2, apel2, jeruk2);
    }
    
    public static void ulang3() {

    	scn3 = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s3 = scn3.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t3 = scn3.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a3 = scn3.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b3 = scn3.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m3 = scn3.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n3 = scn3.nextInt();
    	
    	int [] apel3 = new int[m3];
    	int [] jeruk3 = new int[n3];
    	
    	for (int i3 = 0; i3 < apel3.length; i3++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i3+1) + " : ");
    		int bapel3 = scn3.nextInt();
    		apel3[i3] = bapel3;
    	}
    	
    	for (int j3 = 0; j3 < jeruk3.length; j3++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j3+1) + " : ");
    		int bjeruk3 = scn3.nextInt();
    		jeruk3[j3] = bjeruk3;
    	}
    	
    	countApplesAndOranges(s3, t3, a3, b3, apel3, jeruk3);
    }
    
    public static void ulang4() {

    	scn4 = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s4 = scn4.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t4 = scn4.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a4 = scn4.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b4 = scn4.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m4 = scn4.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n4 = scn4.nextInt();
    	
    	int [] apel4 = new int[m4];
    	int [] jeruk4 = new int[n4];
    	
    	for (int i4 = 0; i4 < apel4.length; i4++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i4+1) + " : ");
    		int bapel4 = scn4.nextInt();
    		apel4[i4] = bapel4;
    	}
    	
    	for (int j4 = 0; j4 < jeruk4.length; j4++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j4+1) + " : ");
    		int bjeruk4 = scn4.nextInt();
    		jeruk4[j4] = bjeruk4;
    	}
    	
    	countApplesAndOranges(s4, t4, a4, b4, apel4, jeruk4);
    }
    
    public static void ulang5() {

    	scn5 = new Scanner(System.in);
    	
    	System.out.print("Titik awal rumah Sam : ");
    	int s5 = scn5.nextInt();
    	
    	System.out.print("Titik akhir rumah Sam : ");
    	int t5 = scn5.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Apel : ");
    	int a5 = scn5.nextInt();
    	
    	System.out.print("Titik lokasi Pohon Jeruk : ");
    	int b5 = scn5.nextInt();
    	
    	System.out.print("Banyaknya Apel : ");
    	int m5 = scn5.nextInt();
    	
    	System.out.print("Banyaknya Jeruk : ");
    	int n5 = scn5.nextInt();
    	
    	int [] apel5 = new int[m5];
    	int [] jeruk5 = new int[n5];
    	
    	for (int i5 = 0; i5 < apel5.length; i5++) {
    		System.out.print("Lokasi Jatuh Apel ke "+ (i5+1) + " : ");
    		int bapel5 = scn5.nextInt();
    		apel5[i5] = bapel5;
    	}
    	
    	for (int j5 = 0; j5 < jeruk5.length; j5++) {
    		System.out.print("Lokasi Jatuh Jeruk ke "+ (j5+1) + " : ");
    		int bjeruk5 = scn5.nextInt();
    		jeruk5[j5] = bjeruk5;
    	}
    	
    	countApplesAndOranges(s5, t5, a5, b5, apel5, jeruk5);
    }

}
