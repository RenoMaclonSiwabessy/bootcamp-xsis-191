package hackerrank.implementation;

public class CatAndMouse {
	

	public static void main(String[] args) {
		int x = 1;
		int y = 3;
		int z = 2;
		int result = Integer.parseInt(catAndMouse(x, y, z));
		System.out.println(result);
	}
	
	static String catAndMouse(int x, int y, int z) {
		if(Math.abs(x - z)< Math.abs(y - z)) {
			return "Cat A";
		}else if(Math.abs(x - z) > Math.abs(y - z)){
			return "Cat B";
		}else {
			return "Mouse C";
		}
	}

}
