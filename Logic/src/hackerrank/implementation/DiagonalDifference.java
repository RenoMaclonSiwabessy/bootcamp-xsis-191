package hackerrank.implementation;

public class DiagonalDifference {
	
    static int diagonalDifference(int[][] arr) {
        int a = 0;
        int b = 0;
        for(int i = 0; i < arr.length; i++){
            a = a + arr[i][i];
            b = b + arr[i][arr.length-1-i];
        }
        if (b > a){
            return b - a;
        }else{
            return a - b;
        }

    }

    public static void main(String[] args) {
       
    	//Membuat elemen dalam array 2 dimensi dengan nama array arr 
    	int[][] arr = new int[3][3];
    	arr[0][0] = 11;
    	arr[0][1] = 2;
    	arr[0][2] = 4;
    	arr[1][0] = 4;
    	arr[1][1] = 5;
    	arr[1][2] = 6;
    	arr[2][0] = 10;
    	arr[2][1] = 8;
    	arr[2][2] = -12;
    	
    	//result memiliki nilai dari return yang terdapat di method diagonalDifference
    	int result = diagonalDifference(arr);
    	System.out.println(result);
    	
    	 ulang1();
         ulang2();
         ulang3();
         ulang4();
         ulang5();
    }
    
    public static void ulang1() {
     	int[][] arr1 = new int[3][3];
     	arr1[0][0] = 11;
     	arr1[0][1] = 2;
     	arr1[0][2] = 4;
     	arr1[1][0] = 4;
     	arr1[1][1] = 5;
     	arr1[1][2] = 6;
     	arr1[2][0] = 10;
     	arr1[2][1] = 8;
     	arr1[2][2] = -12;
     	
     	int result1 = diagonalDifference(arr1);
     	System.out.println(result1);
    	
    }
    
    public static void ulang2() {
    	
     	int[][] arr2 = new int[3][3];
     	arr2[0][0] = 11;
     	arr2[0][1] = 2;
     	arr2[0][2] = 4;
     	arr2[1][0] = 4;
     	arr2[1][1] = 5;
     	arr2[1][2] = 6;
     	arr2[2][0] = 10;
     	arr2[2][1] = 8;
     	arr2[2][2] = -12;
     	
     	//result memiliki nilai dari return yang terdapat di method diagonalDifference
     	int result2 = diagonalDifference(arr2);
     	System.out.println(result2);
    	
    }
    
    public static void ulang3() {
        
     	int[][] arr3 = new int[3][3];
     	arr3[0][0] = 11;
     	arr3[0][1] = 2;
     	arr3[0][2] = 4;
     	arr3[1][0] = 4;
     	arr3[1][1] = 5;
     	arr3[1][2] = 6;
     	arr3[2][0] = 10;
     	arr3[2][1] = 8;
     	arr3[2][2] = -12;
     	
     	int result3 = diagonalDifference(arr3);
     	System.out.println(result3);
    	
    }
    
    public static void ulang4() {

    	
    	int[][] arr4 = new int[3][3];
    	arr4[0][0] = 11;
    	arr4[0][1] = 2;
    	arr4[0][2] = 4;
    	arr4[1][0] = 4;
    	arr4[1][1] = 5;
    	arr4[1][2] = 6;
    	arr4[2][0] = 10;
    	arr4[2][1] = 8;
    	arr4[2][2] = -12;
    	
    	int result4 = diagonalDifference(arr4);
    	System.out.println(result4);
    }
    
    public static void ulang5() {
         
     	int[][] arr5 = new int[3][3];
     	arr5[0][0] = 11;
     	arr5[0][1] = 2;
     	arr5[0][2] = 4;
     	arr5[1][0] = 4;
     	arr5[1][1] = 5;
     	arr5[1][2] = 6;
     	arr5[2][0] = 10;
     	arr5[2][1] = 8;
     	arr5[2][2] = -12;
     	
     	int result5 = diagonalDifference(arr5);
     	System.out.println(result5);
    	
    }

}
