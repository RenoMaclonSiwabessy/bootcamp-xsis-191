package hackerrank.implementation;

import java.util.Scanner;

public class GradingStudent {

	static int[] gradingStudents(int[] grades) {
		
         int[] a = new int[grades.length];

         for (int i = 0; i<grades.length; i++){
             if(grades[i] % 5 > 2 && (grades[i]>=38))
                a[i] = grades[i] + (5 - grades[i] % 5);
            else
                a[i] = grades[i];
         }
         return a;

    }

    static Scanner scan;

    public static void main(String[] args) {
    	scan = new Scanner(System.in);
    	
    	System.out.print("Jumlah Murid : ");
    	int n = scan.nextInt();
    	
    	int[] ar = new int[n];
    	
    	for(int i = 0; i < ar.length; i++) {
    		System.out.print("Nilai Murid " + (i+1) + " = ");
    		int nilai = scan.nextInt();
    		ar[i] = nilai;
    	}
    	
    	int[] hasil = gradingStudents(ar);
    	System.out.print(hasil);
    			
    }

}
