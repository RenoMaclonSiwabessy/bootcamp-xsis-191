package hackerrank.implementation;

import java.util.Scanner;

public class ViralAdvertising {
	
	static Scanner scn;

	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.println("Masukkan N : ");
		int n = scn.nextInt();

	     	int share = 5;
	        int like = 0;
	        for(int i = 0; i < n; i++){
	            share = share / 2;
	            like = like + share;
	            share = share * 3;
	        }

	        System.out.println(like);
	}

}
