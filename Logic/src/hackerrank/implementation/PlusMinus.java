package hackerrank.implementation;

import java.util.Scanner;

public class PlusMinus {
	 static void plusMinus(int[] arr) {
	        
	        double plus = 0;
	        double zero = 0;
	        double minus = 0;
	        
	        for (int i = 0; i < arr.length; i++){
	            if (arr[i] > 0){
	                plus = plus + 1;
	            } 
	            else if (arr[i] == 0){
	                zero = zero + 1;
	            } 
	            else if (arr[i] < 0){
	                minus = minus + 1;
	            } 
	        }
	        
	        System.out.println(plus/arr.length);
	        System.out.println(minus/arr.length);
	        System.out.println(zero/arr.length);

	    }

	   	static Scanner scanner = new Scanner(System.in);
	   	static Scanner scanner1, scanner2, scanner3, scanner4, scanner5 ;

	    public static void main(String[] args) {
	    	
	    	System.out.print("Banyaknya Data : ");
	        int n = scanner.nextInt();
	        
	        //membuat array arr yang banyak datanya sesuai dengan input n 
	        int[] arr = new int[n];
	        
	        //Perulangan untuk melakukan input data ke array arr sebanyak input n
	        for (int i = 0; i < n; i++) {
	        	System.out.print("Data "+(i+1)+" : ");
	            int item = scanner.nextInt();
	            arr[i] = item;
	        }
	        
	        //memanggil method plusMinus
	        plusMinus(arr);
	        
	    }
	    
	    public static void ulang1() {
	    	System.out.print("Banyaknya Data 1 : ");
	        int n1 = scanner1.nextInt();
	        
	        int[] arr1 = new int[n1];
	        
	        for (int i1 = 0; i1 < n1; i1++) {
	        	System.out.print("Data "+ (i1+1) +" : ");
	            int item1 = scanner1.nextInt();
	            arr1[i1] = item1;
	        }
	    }
	    
	    public static void ulang2() {
	    	System.out.print("Banyaknya Data 2 : ");
	        int n2 = scanner.nextInt();
	        
	        int[] arr2 = new int[n2];
	        
	        for (int i2 = 0; i2 < n2; i2++) {
	        	System.out.print("Data "+(i2+1)+" : ");
	            int item2 = scanner.nextInt();
	            arr2[i2] = item2;
	        }
	    }
	    
	    public static void ulang3() {
	    	System.out.print("Banyaknya Data 3 : ");
	        int n3 = scanner3.nextInt();
	        
	        int[] arr3 = new int[n3];
	        
	        for (int i3 = 0; i3 < n3; i3++) {
	        	System.out.print("Data "+i3+" : ");
	            int item3 = scanner.nextInt();
	            arr3[i3] = item3;
	        }
	    }
	    
	    public static void ulang4() {
	    	System.out.print("Banyaknya Data 4 : ");
	        int n4 = scanner4.nextInt();
	        
	        int[] arr4 = new int[n4];
	        
	        for (int i4 = 0; i4 < n4; i4++) {
	        	System.out.print("Data "+i4+" : ");
	            int item4 = scanner.nextInt();
	            arr4[i4] = item4;
	        }
	    }
	    
	    public static void ulang5() {
	    	System.out.print("Banyaknya Data 5 : ");
	        int n5 = scanner5.nextInt();
	        
	        int[] arr5 = new int[n5];
	        
	        for (int i5 = 0; i5 < n5; i5++) {
	        	System.out.print("Data "+i5+" : ");
	            int item5 = scanner.nextInt();
	            arr5[i5] = item5;
	        }
	    }

}
