package hackerrank.implementation;

import java.util.Scanner;

public class Kangaroo {
	
    static String kangaroo(int x1, int v1, int x2, int v2) {
    	//a memiliki tipe data string dengan nilai YES
        String a = "YES";
        if(x1 < x2 && v1 < v2){
            a = "NO";
        }else{
            if(v1 != v2 && (x2 - x1)%(v1-v2)==0){
                a = "YES";
            }else{
                a = "NO";
            }
        }
    return a;
    }

    static Scanner scn, scn1,scn2,scn3,scn4,scn5;
    
    public static void main(String[] args) {
    	
    	scn = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1 : ");
    	int x1 = scn.nextInt();
    	
    	System.out.print("Masukkan V1 : ");
    	int v1 = scn.nextInt();
    	
    	System.out.print("Masukkan X2 : ");
    	int x2 = scn.nextInt();
    	
    	System.out.print("Masukkan V2 : ");
    	int v2 = scn.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result = kangaroo(x1, v1, x2, v2);
        
        System.out.println(result);
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
    
    public static void ulang1() {
    	
    	scn1 = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1a : ");
    	int x1a = scn1.nextInt();
    	
    	System.out.print("Masukkan V1a : ");
    	int v1a = scn1.nextInt();
    	
    	System.out.print("Masukkan X2a : ");
    	int x2a = scn1.nextInt();
    	
    	System.out.print("Masukkan V2a : ");
    	int v2a = scn1.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result1 = kangaroo(x1a, v1a, x2a, v2a);
        
        System.out.println(result1);
    	
    }
    
    public static void ulang2() {
    	
    	scn2 = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1b : ");
    	int x1b = scn2.nextInt();
    	
    	System.out.print("Masukkan V1b : ");
    	int v1b = scn2.nextInt();
    	
    	System.out.print("Masukkan X2b : ");
    	int x2b = scn2.nextInt();
    	
    	System.out.print("Masukkan V2b : ");
    	int v2b = scn2.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result2 = kangaroo(x1b, v1b, x2b, v2b);
        
        System.out.println(result2);
    	
    }
    
    public static void ulang3() {
    	
    	scn3 = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1c : ");
    	int x1c = scn3.nextInt();
    	
    	System.out.print("Masukkan V1c : ");
    	int v1c = scn3.nextInt();
    	
    	System.out.print("Masukkan X2c : ");
    	int x2c = scn3.nextInt();
    	
    	System.out.print("Masukkan V2c : ");
    	int v2c = scn3.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result3 = kangaroo(x1c, v1c, x2c, v2c);
        
        System.out.println(result3);
    	
    }
    
    public static void ulang4() {
    	
    	scn4 = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1d : ");
    	int x1d = scn4.nextInt();
    	
    	System.out.print("Masukkan V1d : ");
    	int v1d = scn4.nextInt();
    	
    	System.out.print("Masukkan X2d : ");
    	int x2d = scn4.nextInt();
    	
    	System.out.print("Masukkan V2d : ");
    	int v2d = scn4.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result4 = kangaroo(x1d, v1d, x2d, v2d);
        
        System.out.println(result4);
    	
    }
    public static void ulang5() {
    	
    	scn5 = new Scanner(System.in);
    	
    	System.out.print("Masukkan X1e : ");
    	int x1e = scn5.nextInt();
    	
    	System.out.print("Masukkan V1e : ");
    	int v1e = scn5.nextInt();
    	
    	System.out.print("Masukkan X2e : ");
    	int x2e = scn5.nextInt();
    	
    	System.out.print("Masukkan V2e : ");
    	int v2e = scn.nextInt();
    	
    	//result memiliki nilai dari proses method kangaroo
        String result5 = kangaroo(x1e, v1e, x2e, v2e);
        
        System.out.println(result5);
    	
    }

}
