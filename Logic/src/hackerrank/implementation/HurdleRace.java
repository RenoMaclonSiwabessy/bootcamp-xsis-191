package hackerrank.implementation;

public class HurdleRace {
	
	public static int hurdleRace(int k, int[] height) {
		int max = height[0];
		
		for (int i = 1; i < height.length; i++) {
			if(height[i]>max) {
				max = height[i];
			}
		}
		if(k < max) {
			return max - k;
		} else {
			return 0;
		}
	}

	public static void main(String[] args) {
		System.out.println(hurdleRace(4, new int[] {1, 6, 3, 5, 2	}));

	}
	
	public static int ulang1(int k1, int[] height1) {
		int max1 = height1[0];
		for (int i1 = 0; i1 < height1.length; i1++) {
			if(height1[i1] > max1) {
				max1 = height1[i1];
			}
		}
		
		if (k1 < max1) {
			return max1 - k1;
		} else {
			return 0;
		}
	}
	
	public static int ulang2(int k2, int[] height2) {
		int max2 = height2[0];
		for (int i2 = 0; i2 < height2.length; i2++) {
			if(height2[i2] > max2) {
				max2 = height2[i2];
			}
		}
		
		if (k2 < max2) {
			return max2 - k2;
		}else {
			return 0;
		}
	}
	
	public static int ulang3(int k3, int[] height3) {
		int max3 = height3[0];
		for (int i3 = 0; i3 < height3.length; i3++) {
			if(height3[i3] > max3) {
				max3 = height3[i3];
			}
		}
		
		if (k3 < max3) {
			return max3 - k3;
		}else {
			return 0;
		}
	}
	
	public static int ulang4(int k4, int[] height4) {
		int max4 = height4[0];
		for (int i4 = 0; i4 < height4.length; i4++) {
			if( height4[i4] > max4 ) {
				max4 = height4[i4];
			}
		}
		
		if(k4 < max4) {
			return max4 - k4;
		}else {
			return 0;
		}
	}
	
	public static int ulang5(int k5, int[] height5) {
		int max5 = height5[0];
		for (int i5 = 0; i5 < height5.length; i5++) {
			if(height5[i5] > max5) {
				max5  = height5[i5];
			}
		}
		
		if (k5 < max5 ) {
			return max5 - k5;
		}else {
			return 0;
		}
	}
	
	public static int ulang6(int k6, int[] height6) {
		int max6 = height6[0];
		for (int i6 = 0; i6 < height6.length; i6++) {
			if(height6[i6] > max6) {
				max6  = height6[i6];
			}
		}
		
		if (k6 < max6 ) {
			return max6 - k6;
		}else {
			return 0;
		}
	}
	
	public static int ulang7(int k7, int[] height7) {
		int max7 = height7[0];
		for (int i7 = 0; i7 < height7.length; i7++) {
			if(height7[i7] > max7) {
				max7  = height7[i7];
			}
		}
		
		if (k7 < max7 ) {
			return max7 - k7;
		}else {
			return 0;
		}
	}

}
