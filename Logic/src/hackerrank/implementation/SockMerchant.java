package hackerrank.implementation;

import java.util.Arrays;
import java.util.Scanner;

public class SockMerchant {
	static Scanner scn;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scn = new Scanner(System.in);
		
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int[] ar = new int[n];
        Arrays.sort(ar);
        for(int i = 0; i < n; i++) {
        	System.out.print("Masukkan Indeks Ke - " + (i+1) + " : ");
        	ar[i] = scn.nextInt();
        }
        
        int a = 0;
        for(int i = 0; i < n-1; i++){ 
            if(ar[i] == ar[i+1]){
                a = a + 1;
                i = i + 1;
            }
           
        }

        System.out.println("Jumlah kaus kaki yang sama : " + a);

	}

}
