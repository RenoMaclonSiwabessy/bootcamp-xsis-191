package hackerrank.implementation;

public class SimpleArraySum {
	
    static int simpleArraySum(int[] ar) {

         int a = 0;
         //ar.length melakukan perulangan sebanyak panjang array
         for(int i = 0; i < ar.length; i++){
             a = a + ar[i];
         }

         return a;

    }


    public static void main(String[] args) {

        int[] ar = new int[] {1, 2, 3, 4, 10, 11};

        //result memiliki nilai hasil return a dari method simpleArraySum(int[] ar)
        int result = simpleArraySum(ar);
        System.out.println(result);
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
    
   public static void ulang1() {

	   int[] ar1 = new int[] {1, 2, 3, 4, 10, 11};

       int result1 = simpleArraySum(ar1);
       System.out.println(result1);
       

   }
   
   public static void ulang2() {


       int[] ar2 = new int[] {1, 2, 3, 4, 10, 11};
       int result2 = simpleArraySum(ar2);
       System.out.println(result2);
       

   }
   
   public static void ulang3() {

       int[] ar3 = new int[] {1, 2, 3, 4, 10, 11};
       
       int result3= simpleArraySum(ar3);
       
       System.out.println(result3);
       

   }
   
   public static void ulang4() {

       int[] ar4 = new int[] {1, 2, 3, 4, 10, 11};
       
       int result4= simpleArraySum(ar4);
       
       System.out.println(result4);
       

   }
   
   public static void ulang5() {

       int[] ar5 = new int[] {1, 2, 3, 4, 10, 11};
       
       int result5= simpleArraySum(ar5);
       
       System.out.println(result5);
       

   }
}
