package hackerrank.implementation;

public class AVeryBigSum {

    static long aVeryBigSum(long[] ar) {

        long a = 0;
        //Membuat perulangan, sebanyak  ar.length banyaknya data di array
        for (int i = 0; i < ar.length; i++){
            a = a + ar[i];
        }

        return a;


    }

    public static void main(String[] args) {
    	//Membuat Array dengan tipe data long dan memiliki elemen sebanyak 5 :
    	//1000000001 1000000002 1000000003 1000000004 1000000005
    	long[] ar = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	
    	//result memiliki nilai hasil return a dari method aVeryBigSum(long[] ar)
    	long result = aVeryBigSum(ar);
    	System.out.println(result);
    	
    	
    	ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
    
    public static void ulang1() {
    	long[] ar1 = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	long result1 = aVeryBigSum(ar1);
    	System.out.println(result1);
    }
    
    public static void ulang2() {
    	long[] ar2 = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	long result2 = aVeryBigSum(ar2);
    	System.out.println(result2);
    }

    public static void ulang3() {
    	long[] ar3 = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	long result3 = aVeryBigSum(ar3);
    	System.out.println(result3);
    }
    
    public static void ulang4() {
    	long[] ar4 = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	long result4 = aVeryBigSum(ar4);
    	System.out.println(result4);
    }
    
    public static void ulang5() {
    	long[] ar5 = new long [] {1000000001, 1000000002, 1000000003, 1000000004, 1000000005};
    	long result5 = aVeryBigSum(ar5);
    	System.out.println(result5);
    }
}
