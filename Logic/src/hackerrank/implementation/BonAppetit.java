package hackerrank.implementation;

import java.util.List;
import java.util.Scanner;

public class BonAppetit {
	
	static Scanner scn,scbill;
	private static List<Integer> bill;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		bonAppetit(bill);

	}
	
	static void bonAppetit(List<Integer> bill) {
		scn = new Scanner(System.in);
		scbill = new Scanner(System.in);
		
		System.out.print("Masukkan Banyaknya Bill : ");
		int n  = scn.nextInt();
		
		System.out.print("Masukkan Indeks Bill Yang Tidak Dibayar : ");
		int k = scn.nextInt();
		
		for(int i = 0; i < n; i++) {
			System.out.println("Bill Index "+i+" : ");
			bill.add(scbill.nextInt());
		}
		
		System.out.println("Masukkan Total Yang Ditagih : ");
		int b = scn.nextInt();
		
		int a = 0;
		int total = 0;
		bill.set(k, 0);
		for (int j = 0; j < bill.size(); j++) {
			a = a + bill.get(j);
		}
		total = b - (a / 2);
		
		if (total == 0) {
			System.out.println("Bon Appetit");
		}else {
			System.out.println(total);
		}
	}

}
