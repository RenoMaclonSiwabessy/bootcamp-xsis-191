package hackerrank.implementation;

import java.util.Scanner;

public class SolveMeFirst {
	
	static Scanner in, in1, in2, in3, in4, in5;
	
	static int solveMeFirst(int a, int b) {
	      return a + b;
	      
		}

	 public static void main(String[] args) {
	     	//Instansiasi in   
		 	in = new Scanner(System.in);
	        
		 	//input a
	        int a;
	        System.out.println("a : ");
	        a = in.nextInt();
	        
	        //input b
	        int b;
	        System.out.println("b : ");
	        b = in.nextInt();
	        
	        int sum;
	        //sum memiliki nilai hasil return a + b method solveMeFirst(int a, int b)
	        sum = solveMeFirst(a, b);
	        System.out.println(sum);
	        
	        ulang1();
	        ulang2();
	        ulang3();
	        ulang4();
	        ulang5();
		}
	 
	 public static void ulang1(){
 
		 	in1 = new Scanner(System.in);

	        int a1;
	        System.out.println("a1 : ");
	        a1 = in1.nextInt();

	        int b1;
	        System.out.println("b1 : ");
	        b1 = in1.nextInt();
	      
	        int sum1;
	        sum1 = solveMeFirst(a1, b1);
	        System.out.println(sum1);
	 }
	 
	 public static void ulang2(){
		 
		 	in2 = new Scanner(System.in);

	        int a2;
	        System.out.println("a2 : ");
	        a2 = in2.nextInt();

	        int b2;
	        System.out.println("b2 : ");
	        b2 = in2.nextInt();
	      
	        int sum2;
	        sum2 = solveMeFirst(a2, b2);
	        System.out.println(sum2);
	 }
	 
	 public static void ulang3(){
		 
		 	in3 = new Scanner(System.in);

	        int a3;
	        System.out.println("a3 : ");
	        a3 = in3.nextInt();

	        int b3;
	        System.out.println("b3 : ");
	        b3 = in3.nextInt();
	      
	        int sum3;
	        sum3 = solveMeFirst(a3, b3);
	        System.out.println(sum3);
	 }

	 public static void ulang4(){
		 
		 	in4 = new Scanner(System.in);

	        int a4;
	        System.out.println("a4 : ");
	        a4 = in4.nextInt();

	        int b4;
	        System.out.println("b4 : ");
	        b4 = in4.nextInt();
	      
	        int sum4;
	        sum4 = solveMeFirst(a4, b4);
	        System.out.println(sum4);
	 }
	 
	 public static void ulang5(){
		 
		 	in5 = new Scanner(System.in);

	        int a5;
	        System.out.println("a5 : ");
	        a5 = in5.nextInt();

	        int b5;
	        System.out.println("b5 : ");
	        b5 = in5.nextInt();
	      
	        int sum5;
	        sum5 = solveMeFirst(a5, b5);
	        System.out.println(sum5);
	 }
}
