package hackerrank.java;

import java.util.Scanner;

public class JavaSubstring {
	
	static Scanner in;

	public static void main(String[] args) {
		in = new Scanner(System.in);
		System.out.print("Masukkan String : ");
        String S = in.next();
        System.out.print("Masukkan Start : ");
        int start = in.nextInt();
        System.out.print("Masukkan End : ");
        int end = in.nextInt();

        System.out.println(S.substring(start,end));

	}

}
