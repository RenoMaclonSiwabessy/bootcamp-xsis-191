package hackerrank.java;

import java.util.Scanner;

public class JavaStaticInitializerBlock {
	
	//Deklarasi type data
	static Scanner scn;
	static boolean flag = true;
    static int B;
    static int H;

    static {
    	//Instansiasi
        scn = new Scanner(System.in);
        //Input Breath
        System.out.print("Masukkan Breath : ");
        B = scn.nextInt();
        
        //Input Height
        System.out.print("Masukkan Height : ");
        H = scn.nextInt();
        
        /*Jika B atau H lebih kecil sama dengan 0, nilai flag menjadi false dan 
        akan mencetak "java.lang.Exception: Breadth and height must be positive"*/
        if (B <= 0 || H <= 0){
            flag = false;
            System.out.print("java.lang.Exception: Breadth and height must be positive");
        }

    }


public static void main(String[] args){
	
		//Jika Flag true maka akan mencetak area yang memiliki nilai dari B * H
		if(flag){
			int area = B * H;
			System.out.println(area);
		}
		
		 ulang1();
	     ulang2();
	     ulang3();
	     ulang4();
	     ulang5();
		
		
		
	}//end of main

	public static void ulang1() {
		if(flag){
			int area = B * H;
			System.out.println(area);
		}
	}
	
	public static void ulang2() {
		if(flag){
			int area = B * H;
			System.out.println(area);
		}
	}
	
	public static void ulang3() {
		if(flag){
			int area = B * H;
			System.out.println(area);
		}
	}
	
	public static void ulang4() {
		if(flag){
			int area = B * H;
			System.out.println(area);
		}
	}
	
	public static void ulang5() {
		if(flag){
			int area = B * H;
			System.out.print(area);
		}
	}
}
