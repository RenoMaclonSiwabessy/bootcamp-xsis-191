package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdout2 {
	static Scanner scan, scn01, scn02, scn03, scn04, scn05;
	
	public static void main(String[] args) {
        scan = new Scanner(System.in);
        System.out.print("Masukkan Integer : ");
        int i = scan.nextInt();
        System.out.print("Masukkan Double : ");
        double d = scan.nextDouble();
        
        /*Menggunakan metode nextLine() setelah metode nextInt(), 
        ingat bahwa nextInt() membaca token integer; karena ini, 
        karakter baris baru terakhir untuk input integer masih 
        dalam antrian di buffer input dan nextLine berikutnya () akan 
        membaca sisa dari integer (yaitu kosong).*/
        scan.nextLine();
        
        System.out.print("Masukkan String : ");
        String s = scan.nextLine();
        

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
	
	public static void ulang1() {
		scn01 = new Scanner(System.in);
		
		System.out.print("Masukkan Integer : ");
		int i1 = scn01.nextInt();
		System.out.print("Masukkan Double : ");
		double d1 = scn01.nextDouble();
		scn01.nextLine();
		System.out.print("Masukkan String : ");
		String s1 = scn01.nextLine();
		
		System.out.println("String : "+ s1);
		System.out.println("Double : "+ d1);
		System.out.println("Int : "+ i1);
	}
	
	public static void ulang2() {
		scn02 = new Scanner(System.in);
		
		System.out.print("Masukkan Integer : ");
		int i2 = scn02.nextInt();
		System.out.print("Masukkan Double : ");
		double d2 = scn02.nextDouble();
		
		scn02.nextLine();
		System.out.print("Masukkan String : ");
		String s2 = scn02.nextLine();
		
		System.out.println("String : "+s2);
		System.out.println("Double : "+d2);
		System.out.println("int : "+i2);
		
	}
	
	public static void ulang3() {
		scn03 = new Scanner(System.in);
		
		System.out.print("Masukkan Integer : ");
		int i3 = scn03.nextInt();
		System.out.print("Masukkan Double : ");
		double d3 = scn03.nextDouble();
		
		scn03.nextLine();
		System.out.print("Masukkan String : ");
		String s3 = scn03.nextLine();
		
		System.out.println("String : "+s3);
		System.out.println("Double : "+d3);
		System.out.println("Int : "+i3);
	}
	
	public static void ulang4() {
		scn04 = new Scanner(System.in);
		
		System.out.print("Masukkan Integer : ");
		int i4 = scn04.nextInt();
		System.out.print("Masukkan Double : ");
		double d4 = scn04.nextDouble();
		
		scn04.nextLine();
		System.out.print("Masukkan String : ");
		String s4 = scn04.nextLine();
		
		System.out.println("String : "+ s4);
		System.out.println("Double : "+ d4);
		System.out.println("Int : "+ i4);
	}
	
	public static void ulang5() {
		scn05 = new Scanner(System.in);
		
		System.out.print("Masukkan Integer : ");
		int i5 = scn05.nextInt();
		System.out.print("Masukkan Double : ");
		double d5 = scn05.nextDouble();
		
		scn05.nextLine();
		System.out.print("Masukkan String : ");
		String s5 = scn05.nextLine();
		
		System.out.println("String : "+s5);
		System.out.println("Double : "+d5);
		System.out.println("Int : "+i5);
	}

}
