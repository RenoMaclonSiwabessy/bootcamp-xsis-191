package hackerrank.java;

import java.util.*;

public class JavaDataTypes {
	
	static Scanner sc, sc1, sc2, sc3, sc4, sc5, sc6;
	
	public static void main(String []argh){
		
	    sc = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t = sc.nextInt();
	
	    for(int i=1;i<=t;i++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke "+i+" : ");
	            long x = sc.nextLong();
	            System.out.println(x+" can be fitted in:");
	            if(x>=-128 && x<=127){
	                System.out.println("* byte");
	            }
	            if(x >= -32768 && x <= 32767){
	                System.out.println("* short");
	            }
	            if(x >= Math.pow(-2,31) && x <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x >= Math.pow(-2,63) && x <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	    
	    ulang1();
	    ulang2();
	    ulang3();
	    ulang4();
	    ulang5();
	}
	
	public static void ulang1() {

		
	    sc1 = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t1 = sc1.nextInt();
	
	    for(int i1 = 1; i1 <= t1; i1++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke "+i1+" : ");
	            long x1 = sc1.nextLong();
	            System.out.println(x1 +" can be fitted in:");
	            if(x1 >= -128 && x1 <= 127){
	                System.out.println("* byte");
	            }
	            if(x1 >= -32768 && x1 <= 32767){
	                System.out.println("* short");
	            }
	            if(x1 >= Math.pow(-2,31) && x1 <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x1 >= Math.pow(-2,63) && x1 <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	}
	
	public static void ulang2() {

		
	    sc2 = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t2 = sc2.nextInt();
	
	    for(int i2 = 1; i2 <= t2; i2++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke " + i2 +" : ");
	            long x2 = sc.nextLong();
	            System.out.println(x2 + " can be fitted in:");
	            if(x2 >= -128 && x2 <= 127){
	                System.out.println("* byte");
	            }
	            if(x2 >= -32768 && x2 <= 32767){
	                System.out.println("* short");
	            }
	            if(x2 >= Math.pow(-2,31) && x2 <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x2 >= Math.pow(-2,63) && x2 <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	}
	
	public static void ulang3() {

		
	    sc3 = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t3 = sc3.nextInt();
	
	    for(int i3 = 1; i3<=t3;i3++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke "+i3+" : ");
	            long x3 = sc.nextLong();
	            System.out.println(x3+" can be fitted in:");
	            if(x3>=-128 && x3<=127){
	                System.out.println("* byte");
	            }
	            if(x3 >= -32768 && x3<= 32767){
	                System.out.println("* short");
	            }
	            if(x3 >= Math.pow(-2,31) && x3 <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x3 >= Math.pow(-2,63) && x3 <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	}
	
	public static void ulang4() {

		
	    sc4 = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t4 = sc4.nextInt();
	
	    for(int i4 = 1; i4 <= t4; i4++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke "+i4+" : ");
	            long x4 = sc.nextLong();
	            System.out.println(x4+" can be fitted in:");
	            if(x4 >= -128 && x4 <= 127){
	                System.out.println("* byte");
	            }
	            if(x4 >= -32768 && x4 <= 32767){
	                System.out.println("* short");
	            }
	            if(x4 >= Math.pow(-2,31) && x4 <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x4 >= Math.pow(-2,63) && x4 <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	}
	
	public static void ulang5() {

		
	    sc5 = new Scanner(System.in);
	    System.out.print("Masukkan Banyaknya Data : ");
	    int t5 = sc5.nextInt();
	
	    for(int i5 = 1; i5 <= t5; i5++){
	
	        try{
	        	System.out.print("Masukkan Angka Ke "+i5+" : ");
	            long x5 = sc.nextLong();
	            System.out.println(x5 +" can be fitted in:");
	            if(x5>=-128 && x5<=127){
	                System.out.println("* byte");
	            }
	            if(x5 >= -32768 && x5 <= 32767){
	                System.out.println("* short");
	            }
	            if(x5 >= Math.pow(-2,31) && x5 <= Math.pow(2,31)-1){
	                System.out.println("* int");
	            }
	            if(x5 >= Math.pow(-2,63) && x5 <= Math.pow(2,63)-1){
	                System.out.println("* long");
	            }
	        }
	        catch(Exception e){
	            System.out.println(sc.next()+" can't be fitted anywhere.");
	        }
	
	    }
	}

}
