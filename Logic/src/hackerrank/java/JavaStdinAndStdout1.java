package hackerrank.java;

import java.util.Scanner;

public class JavaStdinAndStdout1 {
	
	static Scanner scn, scn01, scn02, scn03, scn04, scn05;
	
	public static void main(String[] args) {
        scn = new Scanner(System.in);
        System.out.print("Masukkan A : ");
        int a = scn.nextInt();
        System.out.print("Masukkan B : ");
        int b = scn.nextInt();
        System.out.print("Masukkan C : ");
        int c = scn.nextInt();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        
        ulang1(); 
        ulang2();
        ulang3();
        ulang4();
        ulang5();
    }
	
	public static void ulang1() {
		//Membuat scanner
		scn01  = new Scanner(System.in);
		
		System.out.print("Masukkan A1 : ");
		int a1 = scn01.nextInt();
		System.out.print("Masukkan B1 : ");
		int b1 = scn01.nextInt();
		System.out.print("Masukkan C1 : ");
		int c1 = scn01.nextInt();
		
		System.out.println(a1);
		System.out.println(b1);
		System.out.println(c1);
	}
	
	public static void ulang2() {
		scn02 = new Scanner(System.in);
		
		System.out.print("Masukkan A2 : ");
		int a2 = scn02.nextInt();
		System.out.print("Masukkan B2 : ");
		int b2 = scn02.nextInt();
		System.out.print("Masukkan C2 : ");
		int c2 = scn02.nextInt();
		
		System.out.println(a2);
		System.out.println(b2);
		System.out.println(c2);
	}
	
	public static void ulang3() {
		scn04 = new Scanner(System.in);
		
		System.out.print("Masukkan A3 : ");
		int a3 = scn03.nextInt();
		System.out.print("Masukkan B3 : ");
		int b3 = scn03.nextInt();
		System.out.print("Masukkan C3 : ");
		int c3 = scn03.nextInt();
		
		System.out.println(a3);
		System.out.println(b3);
		System.out.println(c3);
		
	}
	
	public static void ulang4() {
		scn04 = new Scanner(System.in);
		
		System.out.print("Masukkan A4 : ");
		int a4 = scn04.nextInt();
		System.out.print("Masukkan B4 : ");
		int b4 = scn04.nextInt();
		System.out.print("Masukkan C4 : ");
		int c4 = scn04.nextInt();
		
		System.out.println(a4);
		System.out.println(b4);
		System.out.println(c4);
		
	}
	
	public static void ulang5() {
		scn05 = new Scanner(System.in);
		
		System.out.print("Masukkan A5 : ");
		int a5 = scn05.nextInt();
		System.out.print("Masukkan B5 : ");
		int b5 = scn05.nextInt();
		System.out.print("Masukkan C5 : ");
		int c5 = scn05.nextInt();
		
		System.out.println(a5);
		System.out.println(b5);
		System.out.println(c5);
	}
}
