package hackerrank.java;

import java.util.*;

public class JavaIfElse {
	private static final Scanner scanner = new Scanner(System.in);
	static Scanner scn01, scn02, scn03, scn04, scn05;

    public static void main(String[] args) {
    	System.out.print("Masukkan N : ");
        int N = scanner.nextInt();
        
        if(N % 2 != 0) {
        	System.out.println("Weird");
        }else {
        	if(N >= 2 && N <= 5) {
        		System.out.println("Not Weird");
        	}else if(N >= 6 && N <= 20) {
        		System.out.println("Weird");
        	}else {
        		System.out.println("Not Weird");
        	}
        }
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();

    }
    
    public static void ulang1() {
    	scn01 = new Scanner(System.in);
    	
    	System.out.print("Masukkan N1 : ");
    	int n1 = scanner.nextInt();
    	
    	if(n1 % 2 != 0) {
    		System.out.println("Weird");
    	}else {
    		if(n1 <= 2 && n1 >= 5) {
    			System.out.println("Not Weird");
    		}else if(n1 <= 6 && n1 >= 20) {
    			System.out.println("Weird");
    		}else {
    			System.out.println("Not Weird");
    		}
    	}
    	
    }
    
    public static void ulang2() {
    	scn02 = new Scanner(System.in);
    	
    	System.out.print("Masukkan N2 : ");
    	int n2 = scn02.nextInt();
    	
    	if(n2 % 2 != 0) {
    		System.out.println("Weird");
    	}else {
    		if(n2 >= 2 && n2 <= 5) {
    			System.out.println("Not Weird");
    		}else if(n2 >= 6 && n2 <= 20) {
    			System.out.println("Weird");
    		}else {
    			System.out.println("Not Weird");
    		}
    	}
    }
    
    public static void ulang3() {
    	scn03 = new Scanner(System.in);
    	
    	System.out.print("Masukkan N3 : ");
    	int n3 = scn03.nextInt();
    	
    	if(n3 % 2 != 0) {
    		System.out.println("Weird");
    	}else {
    		if(n3 >= 2 && n3 <= 5) {
    			System.out.println("Not Weird");
    		}else if(n3 >= 6 && n3 <= 20) {
    			System.out.println("Weird");
    		}else {
    			System.out.println("Not Weird");
    		}
    	}
    }
    
    public static void ulang4() {
    	scn04 = new Scanner(System.in);
    	
    	System.out.print("Masukkan N4 : ");
    	int n4 = scn04.nextInt();
    	
    	if(n4 % 2 != 0) {
    		System.out.println("Weird");
    	}else {
    		if (n4 >= 2 && n4 <= 5) {
    			System.out.println("Not Weird");
    		}else if(n4 >= 6 && n4 <= 20) {
    			System.out.println("Weird");
    		}else {
    			System.out.println("Not Weird");
    		}
    	}
    	
    }
    
    public static void ulang5() {
    	scn05 = new Scanner(System.in);
    	
    	System.out.print("Masukkan N5 : ");
    	int n5 = scn05.nextInt();
    	
    	if(n5 % 2 == 0) {
    		System.out.println("Weird");
    	}else {
    		if(n5 >= 2 && n5 <= 5) {
    			System.out.println("Not Weird");
    		}else if(n5 >= 6 && n5 <= 20) {
    			System.out.println("Weird");
    		}else {
    			System.out.println("Not Weird");
    		}
    	}
    }
}
