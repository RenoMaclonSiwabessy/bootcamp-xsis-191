package hackerrank.java;

import java.util.*;

public class JavaLoops2 {
	static Scanner in, in1, in2, in3, in4, in5;
	public static void main(String []argh){
		//Input
        in = new Scanner(System.in);
        System.out.print("Masukkan Banyaknya Data : ");
        int t = in.nextInt();
        
        for(int i = 0; i < t; i++){
        	System.out.print("Angka 1 : ");
            int a = in.nextInt();
            System.out.print("Angka 2 : ");
            int b = in.nextInt();
            System.out.print("Panjang Deret : ");
            int n = in.nextInt();

            for(int j = 0; j < n; j++){
                a = a + b;
                if(j > 0) {
                    System.out.print(" ");
                    System.out.print(a);
                }

                b = b * 2;
            }
            System.out.println("");
        }
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
	}
	
	public static void ulang1() {
		//Input 
		in1 = new Scanner(System.in);
		int t1 = in1.nextInt();
		
		for(int k = 0; k < t1; k++) {
			System.out.println("Angka 1 : ");
			int a1 = in1.nextInt();
			System.out.println("Angka 2 : ");
			int b1 = in1.nextInt();
			System.out.println("Panjang Deret : ");
			int n1 = in1.nextInt();
			
			for(int l = 0; l < n1; l++) {
				a1 = a1 + b1;
				if(l > 0) {
					System.out.print(" ");
					System.out.print(a1);
				}
				
				b1 = b1 * 2;
					
			}
			
			System.out.println("");
		}
	}
	
	public static void ulang2() {
		//Input 
		in2 = new Scanner(System.in);
		int t2 = in2.nextInt();
		
		for (int m = 0; m < t2; m++) {
			System.out.print("Angka 1 : ");
			int a2 = in2.nextInt();
			System.out.print("Angka 2 : ");
			int b2 = in2.nextInt();
			System.out.print("Panjang Deret : ");
			int n2 = in2.nextInt();
			
			for(int m1 = 0; m1 < n2; m1++) {
				a2 = a2 + b2;
				if(m1 > 0) {
					System.out.print(" ");
					System.out.print(a2);
				}
				
				b2 = b2 * 2;
			}
			
			System.out.print("");
		}
	}
	
	public static void ulang3() {
		//Input 
		in3 = new Scanner(System.in);
		int t3 = in3.nextInt();
		
		for (int z = 0; z < t3; z++) {
			System.out.print("Angka 1 : ");
			int a3 = in3.nextInt();
			System.out.print("Angka 2 : ");
			int b3 = in3.nextInt();
			System.out.print("Panjang Deret : ");
			int n3 = in3.nextInt();
			
			for(int x = 0; x < n3; x++) {
				a3 = a3 + b3;
				if(x > 0) {
					System.out.print(" ");
					System.out.print(a3);
				}
				
				b3 = b3 * 2;
			}
			
			System.out.print("");
		}
	}
	
	public static void ulang4() {
		//Input 
		in4 = new Scanner(System.in);
		int t4 = in4.nextInt();
		
		for (int r = 0; r < t4; r++) {
			System.out.print("Angka 1 : ");
			int a4 = in4.nextInt();
			System.out.print("Angka 2 : ");
			int b4 = in4.nextInt();
			System.out.print("Panjang Deret : ");
			int n4 = in4.nextInt();
			
			for(int y = 0; y < n4; y++) {
				a4 = a4 + b4;
				if(y > 0) {
					System.out.print(" ");
					System.out.print(a4);
				}
				
				b4 = b4 * 2;
			}
			
			System.out.print("");
		}
	}
	
	public static void ulang5() {
		//Input 
		in5 = new Scanner(System.in);
		int t5 = in5.nextInt();
		
		for (int h = 0; h < t5; h++) {
			System.out.print("Angka 1 : ");
			int a5 = in5.nextInt();
			System.out.print("Angka 2 : ");
			int b5 = in5.nextInt();
			System.out.print("Panjang Deret : ");
			int n5 = in5.nextInt();
			
			for(int j = 0; j < n5; j++) {
				a5 = a5 + b5;
				if(j > 0) {
					System.out.print(" ");
					System.out.print(a5);
				}
				
				b5 = b5 * 2;
			}
			
			System.out.print("");
		}
	}
}
