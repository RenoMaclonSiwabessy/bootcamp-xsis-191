package hackerrank.java;

import java.util.*;

public class JavaLoops1 {
	
	static Scanner scn = new Scanner(System.in);
	static Scanner scn01, scn02, scn03, scn04, scn05;
	
    public static void main(String[] args) {
    	System.out.print("Masukkan Integer : ");
        int N = scn.nextInt();
        int hasil = 0;
        for(int i = 1; i <= 10; i++){
            hasil = N * i;
            System.out.println(N + " x " + i + " = " + hasil);
        }
        
        //Panggil Method
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();

    }
    
    public static void ulang1() {
    	//Membuat input
    	scn01 = new Scanner(System.in);
    	int hasil1 = 0;
    	
    	System.out.print("Masukkan Integer 1 : ");
    	int n1 = scn01.nextInt();
    	
    	//Perulangan
    	for(int i = 1; i <= 10; i++) {
    		hasil1 = n1 * i;
    		System.out.println(n1 + " x " + i + " = " + hasil1);
    	}
    }
    
    public static void ulang2() {
    	//Membuat Input
    	scn02 = new Scanner(System.in);
    	int hasil2 = 0;
    	
    	System.out.println("Masukkan Integer 2 : ");
    	int n2 = scn02.nextInt();
    	
    	//Membuat perulangan
    	for(int j = 0; j <= 10; j++) {
    		hasil2 = n2 * j;
    		System.out.println(n2 + " x " + j + " = " + hasil2);
    	}
    }
    
    public static void ulang3() {
    	//Membuat input
    	scn03 = new Scanner(System.in);
    	System.out.println("Masukkan Integer 3 : ");
    	int n3 = scn03.nextInt();
    	
    	int hasil3 = 0;
    	
    	for (int k = 1; k <= 10; k++) {
    		hasil3 = n3 * k;
    		System.out.println(n3 + " x " + k + " = " + hasil3);
    	}
    }
    
    public static void ulang4() {
    	//Membuat input 
    	scn04 = new Scanner(System.in);
    	System.out.println("Masukkan Integer 4 : ");
    	int n4 = scn04.nextInt();
    	
    	int hasil4 = 0;
    	
    	//Perulangan
    	for (int l = 1; l <= 10; l++) {
    		hasil4 = n4 * l;
    		System.out.println(n4 + " x " + l + " = " + hasil4);
    	}
    }
    
    public static void ulang5() {
    	//Input 
    	scn05 = new Scanner(System.in);
    	System.out.println("Masukkan Integer 5 : ");
    	int n5 = scn05.nextInt();
    	
    	int hasil5 = 0;
    	
    	for (int x = 1; x <= 10; x++) {
    		hasil5 = n5 * x;
    		System.out.println(n5+ " x " + x + " = " + hasil5);
    	}
    }
}
