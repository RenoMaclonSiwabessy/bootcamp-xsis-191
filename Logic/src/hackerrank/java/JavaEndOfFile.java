package hackerrank.java;

import java.util.*;

public class JavaEndOfFile {
	
	static Scanner scn, scn1, scn2, scn3, scn4, scn5;
	
	public static void main(String[] args) {
		
        scn = new Scanner(System.in);
        int i = 1;
        //scn.hasNext akan terus dieksekusi jika terdapat input
        while (scn.hasNext()){
            String s = scn.nextLine();
            System.out.println(i++ +" "+s);
        }
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();

    }
	
	public static void ulang1() {
		
        scn1 = new Scanner(System.in);
        int i1 = 1;
        while (scn1.hasNext()){
            String s1 = scn1.nextLine();
            System.out.println(i1++ +" "+s1);
        }
	}
	
	public static void ulang2() {
		
        scn2 = new Scanner(System.in);
        int i2 = 1;
        while (scn2.hasNext()){
            String s2 = scn2.nextLine();
            System.out.println(i2++ +" "+s2);
        }
	}
	
	public static void ulang3() {
		
        scn = new Scanner(System.in);
        int i3 = 1;
        while (scn3.hasNext()){
            String s3 = scn3.nextLine();
            System.out.println(i3++ +" "+s3);
        }
	}
	
	public static void ulang4() {
		
        scn4 = new Scanner(System.in);
        int i4 = 1;
        while (scn4.hasNext()){
            String s4 = scn4.nextLine();
            System.out.println(i4++ +" "+s4);
        }
	}
	
	public static void ulang5() {
		
        scn5 = new Scanner(System.in);
        int i5 = 1;
        while (scn5.hasNext()){
            String s5 = scn5.nextLine();
            System.out.println(i5++ +" "+s5);
        }
	}

   }

