package hackerrank.java;

import java.util.Scanner;

public class JavaOutputFormatting {
	
	static Scanner sc, scn01, scn02, scn03, scn04, scn05;
	
	public static void main(String[] args) {
		
        sc = new Scanner(System.in);
        
        System.out.println("================================");
        
        for(int i = 0; i < 3; i++){
        	
        	//Melakukan Input String 
            String s1 = sc.next();
            
            //Melakukan Input Integer
            int x = sc.nextInt();
        	
            
            /*printf untuk format 
             %-14s 	: Justify rata kiri dengan banyak karakter 14
             %03d 	: Integer dengan format angka 0 sebanyak 3 digit
             %n 	: Baris baru */
            System.out.printf("%-14s %03d %n", s1, x);
        }
        System.out.println("================================");
        
        ulang1();
        ulang2();
        ulang3();
        ulang4();
        ulang5();
       
	}
	
	public static void ulang1() {
		scn01 = new Scanner(System.in);
		
		for(int j = 0; j < 3; j++) {
			String s1 = scn01.next();
			int i1 = scn01.nextInt();
			
			System.out.printf("%-14s %03d %n", s1, i1);
		}
	}
	
	public static void ulang2() {
		scn02 = new Scanner(System.in);
		
		System.out.println("================================");
		
		for(int k = 0; k < 3; k++) {
			String s2 = scn02.next();
			int i2 = scn02.nextInt();
			
			System.out.printf("%-14s %03d %n", s2, i2);
		}
		
		System.out.println("================================");
	}
	
	public static void ulang3() {
		scn03 = new Scanner(System.in);
		
		System.out.println("================================");
		
		for(int l = 0; l < 3; l++) {
			String s3 = scn03.next();
			int i3 = scn03.nextInt();
			
			System.out.printf("%-14s %03d %n", s3, i3);
		}
		
		System.out.println("================================");
	}
	
	public static void ulang4() {
		scn04 = new Scanner(System.in);
		
		System.out.println("================================");
		
		for (int x = 0; x < 3; x++) {
			String s4 = scn04.next();
			int i4 = scn04.nextInt();
			
			System.out.printf("%-14s %03d %n", s4, i4);
		}
		
		System.out.println("=================================");
	}
	
	public static void ulang5() {
		scn05 = new Scanner(System.in);
		
		System.out.println("=================================");
		
		for(int z = 0; z < 3; z++) {
			String s5 = scn05.next();
			int i5 = scn05.nextInt();
			
			System.out.printf("%-14s %03d %n", s5, i5);
		}
	}
}
