package hackerrank.java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Anagram {

	public static void main(String[] args) {
		System.out.println(isAnagram("Hello", "Hello"));
		System.out.println(isAnagram("Hello", "Helol"));
		System.out.println(isAnagram("Hello", "Heloll"));
		System.out.println(isAnagram("Hello", "lolhe"));
		System.out.println(isAnagram("HelOl", "Ollhe"));

	}
	
	public static boolean isAnagram(String a, String b) {
		Map<String, Integer> map = new HashMap(); 
		String[] a1 = a.toLowerCase().split("");
		String[] b1 = b.toLowerCase().split("");
		for (int i = 0; i<a1.length; i++) {
			if(map.containsKey(a1[i])) {
				int n = map.get(a1[i]);
				n++;
				map.put(a1[i],n);
			} else {
				map.put(a1[i], 1);
			}
		}
		for(int i = 0; i<b1.length;i++) {
			if(!map.containsKey(b1[i])) {
				return false;
			} 
			
			int n = map.get(b1[i]);
			if(n == 0) {
				return false;
			}else {
				n--;
				map.put(b1[i], n);
			}
			
		}
		
		return true;
	}

}
