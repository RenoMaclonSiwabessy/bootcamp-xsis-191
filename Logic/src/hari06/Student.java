package hari06;

public class Student extends Person {
	//List Property
	public String major;
	public double grade;
	
	public Student(int id, String name, String address, String gender, String major, double grade){
		super.id = id;
		super.name = name;
		super.address = address;
		super.gender = gender;
		this.major = major;
		this.grade = grade;
	}
	
	public void showStudent() {
		System.out.println("ID : "+ super.id);
		System.out.println("Nama : "+ super.name);
		System.out.println("Alamat : "+ super.address);
		System.out.println("Gender : "+ super.gender);
		System.out.println("Major : "+ this.major);
		System.out.println("Grade : "+ this.grade);
	}

}
