package hari06;

import java.util.*;

public class MinMaxSum {
	static void miniMaxSum(int[] arr) {
        long max1 = 0;
        long min1 = 0;
        
        Arrays.sort(arr);
        for(int j = 0; j < arr.length-1; j++){
            min1 = min1 + arr[j];
        }

        for(int i = 1; i < arr.length; i++){
            max1 = max1 + arr[i];
        }
        System.out.print(min1 + " "+ max1);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
