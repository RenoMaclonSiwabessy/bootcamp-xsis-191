package hari06;

public class Address {
	//List Properti
	int id;
	String street;
	String city;
	String state;
	String country;
	int zipcode;
	
	public Address(int id, String street, String city, String state, String country, int zipcode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipcode = zipcode;
		
	}

}
