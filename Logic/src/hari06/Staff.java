package hari06;

public class Staff extends Person {
	public int idStaff;
	public int gaji;
	
	public Staff() {
		
	}
	
	public Staff(int id, int idStaff, String name, String address, String gender, int gaji) {
		super(id, name, address, gender);
		this.idStaff = idStaff;
		this.gaji = gaji;
	}
	
	public void showStaff() {
		System.out.println("ID : "+super.id);
		System.out.println("ID Staff : "+this.idStaff);
		System.out.println("Nama : "+super.name);
		System.out.println("Alamat : "+super.address);
		System.out.println("Gender : "+super.gender);
		System.out.println("Gaji : "+this.gaji);
	}

}
