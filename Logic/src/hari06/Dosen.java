package hari06;

public class Dosen extends Person {
	//List Property
	public String MataKuliah;
	public String fakultas;
	
	public Dosen(int id, String name, String address, String gender, String MataKuliah, String fakultas) {
		super(id, name, address, gender);
		this.MataKuliah = MataKuliah;
		this.fakultas = fakultas;
	}
	
	public void showDosen() {
		System.out.println("ID : "+ super.id);
		System.out.println("Nama : "+ super.name);
		System.out.println("Alamat : "+ super.address);
		System.out.println("Gender : "+ super.gender);
		System.out.println("Mata Kuliah : " + this.MataKuliah);
		System.out.println("Fakultas : "+ this.fakultas);
	}
}
