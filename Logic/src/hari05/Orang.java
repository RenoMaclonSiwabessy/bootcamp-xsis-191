package hari05;

public class Orang {
	//Property
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	
	//Constructor
	public Orang() {
		
	}
	
	//constructor
	public Orang(int id, String nama, String alamat, String jk, int umur) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.umur = umur;
	}
	
	//Constructor
	public Orang(int id, String nama, String alamat) {
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
	}
	
	//Constructor bisa di buat lebih dari 1
	//this. bisa method atau property
	//Contructor = method yang dipanggil pada saat instansiasi
	
	//Method
	public void showData() {
		System.out.println("ID \t:"+ this.id);
		System.out.println("Nama \t:"+ this.nama);
		System.out.println("Alamat \t:"+ this.alamat);
		System.out.println("JK\t:"+ this.jk);
		System.out.println("Umur \t:"+ this.umur);
		System.out.println("\n");
	}

}
