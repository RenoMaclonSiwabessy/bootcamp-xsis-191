package hari05;

public class Game {
	//Property
	int id;
	String nama;
	String genre;
	int jumlah;
	
	//Constructor Game Default
	public Game() {
		
	}
	
	// Constructor Game 1
	public Game(int id, String nama, String genre, int jumlah) {
		this.id = id;
		this.nama = nama;
		this.genre = genre;
		this.jumlah = jumlah;
	}
	
	// Consturctor Game 2
	public Game(String nama, String genre) {
		this.nama = nama;
		this.genre = genre;
	}
	
	// Constructor Game 3
	public Game(int id, String nama, int jumlah) {
		this.id = id;
		this.nama = nama;
		this.jumlah = jumlah;
	}
	
	//Method
	public void showGame() {
		System.out.println("ID : "+this.id);
		System.out.println("Judul Film : "+this.nama);
		System.out.println("Genre : "+this.genre);
		System.out.println("Jumlah Game : "+this.jumlah);
	}

}
