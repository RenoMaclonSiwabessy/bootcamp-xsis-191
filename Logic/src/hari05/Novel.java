package hari05;

public class Novel {
	int id;
	String judul;
	String genre;
	String author;
	int tahun;
	
	public Novel(){
		
	}
	
	public Novel(int id, String judul, String genre, String author, int tahun) {
		this.id = id;
		this.judul = judul;
		this.genre = genre;
		this.author = author;
		this.tahun = tahun;
	}
	
	public void showNovel() {
		System.out.println("ID : "+this.id);
		System.out.println("Judul : "+this.judul);
		System.out.println("Genre : "+this.genre);
		System.out.println("Author : "+this.author);
		System.out.println("Tahun : "+this.tahun);
	}
	
}
