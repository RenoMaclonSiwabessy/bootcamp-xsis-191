package hari05;


public class AVeryBigSum {
	static long aVeryBigSum(long[] ar) {
        long a = 0;
        for(int i = 0; i < ar.length; i++){
            a = a + ar[i];
        }

        return a;

    }
	
	public static void main(String[] args) {
		long[] arr = new long [] {12, 13, 24, 25, 22};
		
		long cetak = aVeryBigSum(arr);
		
		System.out.println(cetak);
	}
}
