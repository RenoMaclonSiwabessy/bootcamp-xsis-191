package hari05;

import java.util.*;

public class PlusMinus {
	 static void plusMinus(int[] arr) {
	        
	        double plus = 0;
	        double zero = 0;
	        double minus = 0;
	        
	        for (int i = 0; i < arr.length; i++){
	            if (arr[i] > 0){
	                plus += 1;
	            } 
	            else if (arr[i] == 0){
	                zero += 1;
	            } 
	            else if (arr[i] < 0){
	                minus += 1;
	            } 
	        }
	        
	        System.out.println(plus/arr.length);
	        System.out.println(minus/arr.length);
	        System.out.println(zero/arr.length);

	    }

	    private static final Scanner scanner = new Scanner(System.in);

	    public static void main(String[] args) {
	        int n = scanner.nextInt();
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

	        int[] arr = new int[n];

	        String[] arrItems = scanner.nextLine().split(" ");
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

	        for (int i = 0; i < n; i++) {
	            int arrItem = Integer.parseInt(arrItems[i]);
	            arr[i] = arrItem;
	        }

	        plusMinus(arr);

	        scanner.close();
	    }
}
