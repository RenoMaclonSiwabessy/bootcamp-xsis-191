package hari05;

public class GameMain {

	public static void main(String[] args) {
		Game g1 = new Game(1, "Mario Bros", "Adventure", 5);
		System.out.println("Game 1 :");
		g1.showGame();
		
		Game g2 = new Game(2, "Mortal Kombat", "Action", 6);
		System.out.println("Game 2 : ");
		g2.showGame();

	}

}
