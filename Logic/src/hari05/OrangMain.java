package hari05;

public class OrangMain {

	public static void main(String[] args) {
		//new = alokasi memori baru
		
		//Orang
		Orang org1 = new Orang(1, "Ahmad Roni", "Jakarta", "Pria", 20);
		System.out.println("Data Orang 1 : ");
		org1.showData();
		
		Orang org2 = new Orang(2, "Shella", "Depok Solo");
		System.out.println("Data Orang 2 : ");
		org2.showData();
		
		Orang org3 = new Orang();
		System.out.println("Data Orang 3 : ");
		org3.showData();
		
		Orang org4 = org1;
		System.out.println("Data Orang 4 :");
		org4.nama = "Shifa";
		org4.showData();
		
		System.out.println("Data Orang 1 : ");
		org1.showData();
		
		Orang org5 = org4;
		System.out.println("Data Orang 5 :");
		org5.jk = "Wanita Sholehah";
		org5.showData();
		
		System.out.println("");
		
		//Rumah
		
		Rumah r1 = new Rumah(20,10);
		System.out.println("Data Rumah 1 : ");
		r1.showRumah();

		System.out.println("");
		
		Rumah r2 = r1;
		System.out.println("Data Rumah 2 : ");
		r2.p = 40;
		r2.kamar = 5;
		r2.kamarMandi = 3;
		r2.showRumah();
		
	}

}
