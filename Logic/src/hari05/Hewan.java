package hari05;

public class Hewan {
	//property
	int id;
	String nama;
	String jk;
	
	//Constructor Default Hewan
	public Hewan() {
		
	}
	
	//Constructor
	public Hewan(int id, String nama, String jk) {
		this.id = id;
		this.nama = nama;
		this.jk = jk;
	}
	
	//Constuctor
	public Hewan(String nama, String jk) {
		this.nama = nama;
		this.jk = jk;
	}
	
	//Constructor
	public Hewan(int id, String nama) {
		this.id = id;
		this.nama = nama;
	}
	
	//Method
	public void showHewan() {
		System.out.println("ID :\t"+this.id);
		System.out.println("Nama :\t"+this.nama);
		System.out.println("Jenis Kelamin :\t"+this.jk);
		System.out.println("\n");
	}

}
