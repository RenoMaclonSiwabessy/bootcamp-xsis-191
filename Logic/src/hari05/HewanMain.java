package hari05;

public class HewanMain {

	public static void main(String[] args) {
		Hewan h1 = new Hewan(1, "Anjing", "Male");
		System.out.println("Data Hewan 1 : ");
		h1.showHewan();
		
		Hewan h2 = new Hewan(2, "Sapi", "Male");
		System.out.println("Data Hewan 2 : ");
		h2.showHewan();
		
		Hewan h3 = new Hewan(3, "Kambing", "Female");
		System.out.println("Data Hewan 3 : ");
		h3.showHewan();
		

	}

}
