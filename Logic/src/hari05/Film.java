package hari05;

public class Film {
	
	//Properti
	int id;
	String judul;
	String genre;
	int jumlah;
	int tahun;
	
	//construtor
	public Film(int id, String judul, String genre, int jumlah, int tahun) {
		this.id = id;
		this.judul = judul;
		this.genre = genre;
		this.jumlah = jumlah;
		this.tahun = tahun;
	}
	
	//constructor
	public Film(int id, String judul, int tahun) {
		this.id = id;
		this.judul = judul;
		this.tahun = tahun;
	}
	
	//constructor
	public Film(String judul, String genre, int tahun) {
		this.judul = judul;
		this.genre = genre;
		this.tahun = tahun;
	}
	
	//Method
	public void showFilm() {
		System.out.println("ID : "+this.id);
		System.out.println("Judul : "+this.judul);
		System.out.println("Genre : "+this.genre);
		System.out.println("Jumlah : "+this.jumlah);
		System.out.println("Tahun : "+this.tahun);
	}
	

}
