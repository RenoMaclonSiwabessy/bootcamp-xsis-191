package hari05;

public class Rumah {
	//property
	int p;
	int l;
	int kamar;
	int jendela;
	int pintu;
	int kamarMandi;
	
	public Rumah(int p, int l, int kamar, int jendela, int pintu, int kamarMandi) {
		this.p = p;
		this.l = l;
		this.kamar = kamar;
		this.jendela = jendela;
		this.pintu = pintu;
		this.kamarMandi = kamarMandi;
	}
	
	public Rumah(int p, int l) {
		this.p = p;
		this.l = l;
	}
	
	public Rumah(int kamar, int jendela, int pintu, int kamarMandi) {
		this.kamar = kamar;
		this.jendela = jendela;
		this.pintu = pintu;
		this.kamarMandi = kamarMandi;
	}
	
	public void showRumah() {
		System.out.println("Panjang :"+ this.p);
		System.out.println("Lebar : "+ this.l);
		System.out.println("Kamar : "+ this.kamar);
		System.out.println("Jendela : "+ this.jendela);
		System.out.println("Pintu : "+ this.pintu);
		System.out.println("Kamar Mandi : "+ this.kamarMandi);
		System.out.println("\n");
	}

}
