package hari03;

import java.util.Scanner;

import common.DeretAngka;
import common.PrintArray;

public class Logic03Soal02 {
	
	static Scanner scn;

	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		//1. Membuat array deret
		int[] deret = DeretAngka.deret01(n*4, m, o);
		//2. Buat array 2 dimensi
		String[][] array = new String[n][n];
		//4. Mmebuat index
		int index = 0;
		
		for (int i = 0; i < n; i--) {
			array[i][n-1-i] = "";
			index++;
			
		}
		
		//menampilkannya
		PrintArray.array2D(array);
	}

}
