package hari03;

public class Array1D {
	public static void main(String[] args) {
		//Deklarasi array
		//Index nya dari 0 - 9
		int[] array = new int[10];
		//mengisi value element array
		array[0]=1;//deklarasi variable
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		array[5]=6;
		array[6]=7;
		array[7]=8;
		array[8]=9;
		array[9]=10;
		System.out.println(array.length);
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		System.out.println();
		
		//Cara Kedua
		
		//Instansiasi
		int[] array2 = new int[] {1,2,3,4,5};
		//Panjang Array
		System.out.println(array2.length);
		//For 
		for(int j = 0; j < array2.length; j++) {
			System.out.println(array2[j]);
		}
		
		
	}

}
