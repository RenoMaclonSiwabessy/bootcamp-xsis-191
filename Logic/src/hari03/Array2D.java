package hari03;

public class Array2D {

	public static void main(String[] args) {
		int[][] array2 = new int[5][3];
		//mengisi
		for (int i = 0; i < array2.length; i++) {
			int angka =1;
			for(int j = 0; j < array2[i].length; j++) {
				array2[i][j]=angka;
				angka += 3;
			}
		}
		//menampilkannya
		for(int a = 0; a < array2.length; a++) {
			//print dari kiri ke kanan
			for(int b = 0; b < array2[a].length; b++) {
				System.out.print(array2[a][b]+"\t");
			}
			//pindah baris
			System.out.println("\n");
		}

	}

}
