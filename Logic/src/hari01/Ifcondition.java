package hari01;

public class Ifcondition {
	public static void main(String[] args) {
		int x=10;
		if (x / 5 == 2) {
			System.out.println("X / 5 => 2");
		} else if (x * 2 == 20) {
			System.out.println("X * 2 => 20");
		} else if (x % 2 == 0) {
			System.out.println("X % 2 => 0");
		} else if (x / 5 == x % 5) {
			System.out.println("X / 5 == X % 5");
		} else {
			System.out.println("Else");
		}
	}

}
