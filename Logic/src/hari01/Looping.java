package hari01;

import java.util.Scanner;

public class Looping {
	//Membuat Objek scn
	static Scanner scn;
	
	//Method main
	public static void main(String[] args) {
		//Instansiasi
		scn = new Scanner(System.in);
		System.out.println("Masukkan Nilai n : ");
		int n = scn.nextInt();
		//3 1 9 3 15 5 21 7
		int a = 3; int b = 1;
		for (int i = 0; i<n; i++) {
			System.out.print(a+",");
			System.out.print(b+",");
			a+=6; b+=2;
		}
		
		/*int a = 100;
		int b = 50;
		int hasil1 = tambah(a,b);
		int hasil2 = kali(a,b);
		float hasil3 = bagi(a,b);
		int hasil4 = kurang(a,b);
		System.out.println("Hasil Tambah : " + hasil1 );
		System.out.println("Hasil Kali : " + hasil2);
		System.out.println("Hasil Bagi : " + hasil3);
		System.out.println("Hasil Kurang : " + hasil4);
		int n = scn.nextInt();*/
	}
	
	static int tambah(int x, int y) {
		return x+y;
	}
	
	static int kali(int m, int n) {
		return m*n;
	}
	
	static int kurang(int a, int b) {
		return a-b;
	}
	
	static float bagi(int c, int d) {
		return c/d;
	}
	
	static void inputOutput() {
		//Tampilkan di layar
		System.out.println("Masukkan Nama : ");
		//Membuat Variable
		String nama = scn.nextLine();
		//Menampilkan variable di layar
		System.out.println("Nama : "+ nama);
	}

}
