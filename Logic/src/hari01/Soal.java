package hari01;

import java.util.Scanner;

public class Soal {
	
	static Scanner scn;

	public static void main(String[] args) {
		//Panggil Method
		System.out.println("Soal 1"); soal01();
		System.out.println("");
		System.out.println("Soal 2"); soal02();
		System.out.println("");
		System.out.println("Soal 3"); soal03();
		System.out.println("");
		System.out.println("Soal 4"); soal04();
		System.out.println("");
		System.out.println("Soal 5"); soal05();
		System.out.println("");
		System.out.println("Soal 6"); soal06();
		System.out.println("");
		System.out.println("Soal 7"); soal07();
		System.out.println("");
		System.out.println("Soal 8"); soal08();
		System.out.println("");
		System.out.println("Soal 9"); soal09();
		System.out.println("");
		System.out.println("Soal 10"); soal10();
		}
		
	public static void soal01() {
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 1;
		
		
		//Perulangan
		for (int i=0; i<n; i++) {
			System.out.print(c + " ");
			c = c + 2;
		}
	}
	
	public static void soal02() {
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 2;
		
		//Perulangan
		for (int i=0; i < n; i++) {
			System.out.print(c+" ");
			c =  c + 2;
		}
	}
	
	public static void soal03() {
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 1;
		
		//Perulangan
		for (int i = 0; i < n; i++) {
			System.out.print(c + " ");
			c = c + 3;
		}
	}
	
	public static void soal04() {
		//Input
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 1;
		
		//Perulangan
		for (int i = 0; i < n; i++) {
			System.out.print(c + " ");
			c = c + 3;
		}
	}
	
	public static void soal05() {
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 1;
		for (int i = 1; i <= n; i++) {
			if(i %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(c + " ");
				c = c + 4;
			}	
		}
	}
	
	public static void soal06() {
		//Input
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 1;
		
		//Perulangan
		for (int i = 1; i <= n; i++) {
			
			if(i % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c + " ");
			}
			
			c = c + 4;
			
		}
	}
	
	public static void soal07() {
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 2;
		
		//Perulangan
		for (int i = 0; i < n; i++) {
				System.out.print(c + " ");
				c = c * 2; 
			}	
		}
	
	public static void soal08() {
		
		//Input 
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 3;
		
		//Perulangan 
		for (int i = 0; i < n; i++) {
				System.out.print(c + " ");
				c = c * 3; 
			}	
		}
	public static void soal09() {
		//Input
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 4;
		
		//Perulangan
		for (int i = 1; i <= n; i++) {
			if(i % 3 == 0){
				System.out.print(" * " + " ");
			} else {
				System.out.print(c + " ");
				c = c * 4; 
			}
		}
		}
	
	public static void soal10() {
		//Input
		scn = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		
		int c = 3;
		//Perulangan
		for (int i = 1; i <= n; i++) {
			if (i % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(c + " ");
			}
			c = c * 3;
		}
	}

}
