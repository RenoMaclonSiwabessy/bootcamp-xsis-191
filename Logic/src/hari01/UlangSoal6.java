package hari01;

import java.util.Scanner;

public class UlangSoal6 {
	
	static Scanner scn01, scn02, scn03, scn04, scn05;

	public static void main(String[] args) {
		ulang1();
		System.out.println("\n");
		ulang2();
		System.out.println("\n");
		ulang3();
		System.out.println("\n");
		ulang4();
		System.out.println("\n");
		ulang5();
		System.out.println("\n");

	}
	
	public static void ulang1() {
		//Input
		scn01 = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n1 = scn01.nextInt();
		
		int c1 = 1;
		
		//Perulangan
		for (int i = 1; i <= n1; i++) {
			
			if(i % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c1 + " ");
			}
			
			c1 = c1 + 4;
			
		}
	}
	
	public static void ulang2() {
		//Input
		scn02 = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n2 = scn02.nextInt();
		
		int c2 = 1;
		
		//Perulangan
		for (int j = 1; j <= n2; j++) {
			
			if(j % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c2 + " ");
			}
			
			c2 = c2 + 4;
			
		}
	}
	
	public static void ulang3() {
		//Input
		scn03 = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n3 = scn03.nextInt();
		
		int c3 = 1;
		
		//Perulangan
		for (int k = 1; k <= n3; k++) {
			
			if(k % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c3 + " ");
			}
			
			c3 = c3 + 4;
			
		}
	}
	
	public static void ulang4() {
		//Input
		scn04 = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n4 = scn04.nextInt();
		
		int c4 = 1;
		
		//Perulangan
		for (int l = 1; l <= n4; l++) {
			
			if(l % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c4 + " ");
			}
			
			c4 = c4 + 4;
			
		}
	}
	
	public static void ulang5() {
		//Input
		scn05 = new Scanner(System.in);
		System.out.print("Masukkan N5 : ");
		int n5 = scn05.nextInt();
		
		int c5 = 1;
		
		//Perulangan
		for (int z = 1; z <= n5; z++) {
			
			if(z % 3 == 0) {
				System.out.print(" * " + " ");
			}else {
				
				System.out.print(c5 + " ");
			}
			
			c5 = c5 + 4;
			
		}
	}
}
