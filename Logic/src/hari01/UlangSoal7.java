package hari01;

import java.util.Scanner;

public class UlangSoal7 {
	
	static Scanner scn01, scn02, scn03, scn04, scn05;

	public static void main(String[] args) {
		ulang1();
		System.out.println("\n");
		ulang2();
		System.out.println("\n");
		ulang3();
		System.out.println("\n");
		ulang4();
		System.out.println("\n");
		ulang5();
		System.out.println("\n");

	}
	
	public static void ulang1() {
		//Input 
		scn01 = new Scanner(System.in);
		System.out.print("Masukkan N : ");
		int n1 = scn01.nextInt();
		
		int a = 2;
		
		//Perulangan
		for (int i = 0; i < n1; i++) {
				System.out.print(a + " ");
				a = a * 2; 
			}	
	}
	
	public static void ulang2() {
		//Input 
		scn02 = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n2 = scn02.nextInt();
		
		int b = 2;
		
		//Perulangan
		for (int j = 0; j < n2; j++) {
				System.out.print(b + " ");
				b = b * 2; 
			}	
	}
	
	public static void ulang3() {
		//Input 
		scn03 = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n3 = scn03.nextInt();
		
		int c = 2;
		
		//Perulangan
		for (int i = 0; i < n3; i++) {
				System.out.print(c + " ");
				c = c * 2; 
			}	
	}
	
	public static void ulang4() {
		//Input 
		scn04 = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n4 = scn04.nextInt();
		
		int d = 2;
		
		//Perulangan
		for (int l = 0; l < n4; l++) {
				System.out.print(d + " ");
				d = d * 2; 
			}	
	}
	
	public static void ulang5() {
		//Input 
		scn05 = new Scanner(System.in);
		System.out.print("Masukkan N5 : ");
		int n5 = scn05.nextInt();
		
		int e = 2;
		
		//Perulangan
		for (int z = 0; z < n5; z++) {
				System.out.print(e + " ");
				e = e * 2; 
			}
	}
}
