package hari01;

import java.util.Scanner;

public class UlangSoal5 {
	
	static Scanner scn01, scn02, scn03, scn04, scn05;

	public static void main(String[] args) {
		ulang1();
		System.out.println("\n");
		ulang2();
		System.out.println("\n");
		ulang3();
		System.out.println("\n");
		ulang4();
		System.out.println("\n");
		ulang5();
		System.out.println("\n");

	}
	
	public static void ulang1() {
		//Input 
		scn01 = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n1 = scn01.nextInt();
				
		int a = 1;
		for (int i = 1; i <= n1; i++) {
			if(i %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(a + " ");
				a = a + 4;
			}	
		}
	}
	
	public static void ulang2() {
		//Input 
		scn02 = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n2 = scn02.nextInt();
				
		int b = 1;
		for (int j = 1; j <= n2; j++) {
			if(j %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(b + " ");
				b = b + 4;
			}	
		}
	}
	
	public static void ulang3() {
		//Input 
		scn03 = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n3 = scn03.nextInt();
				
		int c = 1;
		for (int k = 1; k <= n3; k++) {
			if(k %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(c + " ");
				c = c + 4;
			}	
		}	
	}
	
	public static void ulang4() {
		//Input 
		scn04 = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n4 = scn04.nextInt();
				
		int d = 1;
		for (int l = 1; l <= n4; l++) {
			if(l %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(d + " ");
				d = d + 4;
			}	
		}
	}
	
	public static void ulang5() {
		//Input 
		scn05 = new Scanner(System.in);
		System.out.print("Masukkan N5 : ");
		int n5 = scn05.nextInt();
				
		int e = 1;
		for (int x = 1; x <= n5; x++) {
			if(x %  3 == 0) {
				System.out.print(" * " + " ");
			}else {
				System.out.print(e + " ");
				e = e + 4;
			}	
		}
	}

}
