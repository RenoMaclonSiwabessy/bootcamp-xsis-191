package hari01;

import java.util.Scanner;

public class UlangSoal10 {
	
	static Scanner scn01, scn02, scn03, scn04, scn05;

	public static void main(String[] args) {
		ulang1();
		System.out.println("\n");
		ulang2();
		System.out.println("\n");
		ulang3();
		System.out.println("\n");
		ulang4();
		System.out.println("\n");
		ulang5();
		System.out.println("\n");

	}
	
	public static void ulang1() {
		//Input
		scn01 = new Scanner(System.in);
		System.out.print("Masukkan N1 : ");
		int n1 = scn01.nextInt();
		
		int a = 3;
		//Perulangan
		for (int i = 1; i <= n1; i++) {
			if (i % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(a + " ");
			}
			a = a * 3;
		}
	}
	
	public static void ulang2() {
		//Input
		scn02 = new Scanner(System.in);
		System.out.print("Masukkan N2 : ");
		int n2 = scn02.nextInt();
		
		int b = 3;
		//Perulangan
		for (int j = 1; j <= n2; j++) {
			if (j % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(b + " ");
			}
			b = b * 3;
		}
	}
	
	public static void ulang3() {
		//Input
		scn03 = new Scanner(System.in);
		System.out.print("Masukkan N3 : ");
		int n3 = scn03.nextInt();
		
		int c = 3;
		//Perulangan
		for (int k = 1; k <= n3; k++) {
			if (k % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(c + " ");
			}
			c = c * 3;
		}
	}
	
	public static void ulang4() {
		//Input
		scn04 = new Scanner(System.in);
		System.out.print("Masukkan N4 : ");
		int n4 = scn04.nextInt();
		
		int d = 3;
		//Perulangan
		for (int l = 1; l <= n4; l++) {
			if (l % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(d + " ");
			}
			d = d * 3;
		}
	}
	
	public static void ulang5() {
		//Input
		scn05 = new Scanner(System.in);
		System.out.print("Masukkan N5 : ");
		int n5 = scn05.nextInt();
		
		int e = 3;
		//Perulangan
		for (int z = 1; z <= n5; z++) {
			if (z % 4 == 0) {
				System.out.print("XXX"+" ");
			} else {
				System.out.print(e + " ");
			}
			e = e * 3;
		}
	}
}
