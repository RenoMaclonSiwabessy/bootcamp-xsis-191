package test;

import java.util.Scanner;

public class Soal02 {
	
	static Scanner scn;

	public static void main(String[] args) {
		String kata1 = "";
		String kata2 = "";
		
		scn = new Scanner(System.in);
		
		System.out.print("Masukkan Kata : ");
		kata1 = scn.nextLine();
		
		int panjang  = kata1.length();
		
		for (int i = panjang-1; i >= 0; i--) {
			kata2 = kata2 + kata1.charAt(i);
		}
		
		if (kata1.equals(kata2)) {
			System.out.println("YES");
		}else {
			System.out.println("NO");
		}
	}

}
