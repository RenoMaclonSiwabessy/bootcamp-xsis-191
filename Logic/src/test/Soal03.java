package test;

import java.util.ArrayList;
import java.util.Scanner;

public class Soal03 {
	
	static Scanner scn;

	public static void main(String[] args) {
		
		scn = new Scanner(System.in);
		
		System.out.print("Masukkan jumlah data : ");
		int jmlData = scn.nextInt();
		
		ArrayList<Integer> bilanganBulat = new ArrayList<Integer>();
		for (int i = 0; i < jmlData; i++) {
			System.out.print("Masukkan data ke "+(i+1)+" : ");
			bilanganBulat.add(scn.nextInt());
		}
		
		System.out.print("Rotasi sebanyak : ");
		int n = scn.nextInt();
		
		for (int i = 0; i < n; i++) {
			bilanganBulat.set(i, i+1);
		}
		

		System.out.println(bilanganBulat);
	}

}
