package test;

public class Soal01 {

	public static void main(String[] args) {
		belanja();
	}
	
	static int getMoneySpent(int[] keyboards, int[] drives, int budget) {
		int max = -1;

		for (int i = 0; i < keyboards.length; i++) {
			for (int j = 0; j < drives.length; j++) {
				if (keyboards[i] + drives[j] <= budget && keyboards[i] + drives[j] > max)
					max = keyboards[i] + drives[j];
			}
		}
		return max;
	}
	
	public static void belanja() {
		int uangAndi = 50;
		int[] kacamata = new int[] {26,24,38};
		int[] baju = new int[] {28,30,27};
		int max = 0;
		
		for (int i = 0; i < kacamata.length; i++) {
			for (int j = 0; j < baju.length; j++) {
				if (kacamata[i] + baju[j] <= uangAndi && kacamata[i] + baju[j] > max) {
					max = kacamata[i] + baju[j];
				}else {
					System.out.println("Dana Tidak Mencukupi");
				}
			}

			System.out.println(max);
			
		}
		
		
	}

}
