package test;

import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;


public class Soal07 {

	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan banyaknya data : ");
        int size = scan.nextInt();
        int [] array = new int[size];
        for (int i = 0; i < size; i++) {
        	System.out.print("Masukkan data ke - "+ (i+1) + " : ");
            array[i] = scan.nextInt();
        }
        scan.close();
        
        Arrays.sort(array);
        
        /* Mean*/
        int total = 0;
        for (int num : array) {
            total += num;
        }
        double mean = (double) total / size;
        
        /* Median */
        double median;
        if (size % 2 == 0) {
            median = (array[size / 2 - 1] + array[size / 2]) / 2.0;
        } else {
            median = array[size / 2];
        }
        
        /* modus */
        HashMap<Integer, Integer> map = new HashMap<>();
        int max = 0;
        int modus = Integer.MAX_VALUE;
        for (int num : array) {
            map.merge(num, 1, Integer::sum);
            int b = map.get(num);
            if (b > max || (b == max && num < modus)) {
                max = b;
                modus = num;
            }
        }

        /* Print results */
        System.out.println("Mean : "+mean);
        System.out.println("Median : "+median);
        System.out.println("Modus : "+ modus);
	}
}
