package hari02;

import java.util.Scanner;

public class Soal06 {
	
	static Scanner scn;

	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.print("Input : ");
		String text = scn.next();
		
		int a = text.length();
		int b = 0;
		for (int i = 0; i<a; i++) {
			if (Character.isUpperCase(text.charAt(i))) {
				b++;
			}
		}
		System.out.println("Output : "+b);
	}

}
