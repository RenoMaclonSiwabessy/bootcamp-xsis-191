package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.EmployeeModel;
import com.xsis.demo.repository.EmployeeRepo;

@Controller
public class EmployeeController {
	//Membuat auto instance dari repository
	@Autowired
	private EmployeeRepo repo;
	
	//Request yang ada di url localhost:port/employee/index
	@RequestMapping("employee/index")
	public String index(Model model) {
		//Membuat object list employee, kemudian diisi dari object repo dengan method finAll
		List<EmployeeModel> data = repo.findAll();
		
		//Mengirim variable list data, valuenya diisi dari object data
		model.addAttribute("listData", data);
		
		//Menampilkan view /src/main/resources/templates/
		return "employee/index";
	}
	
	//Request yang ada di url localhost:port/employee/add
	@RequestMapping("/employee/add")
	public String add() {
		//Menampilkan view /src/main/resources/templates/employee/add
		return "employee/add";
	}
	//Request yang ada di url localhost:port/employee/save
	//Memiliki method post
	@RequestMapping(value = "/employee/save", method = RequestMethod.POST)
	public String save(@ModelAttribute EmployeeModel item) {
		//Simpan ke database
		repo.save(item);
		
		//Redirecr : akan diteruskan ke halaman index
		return "redirect:/employee/index";
	}
	
}
