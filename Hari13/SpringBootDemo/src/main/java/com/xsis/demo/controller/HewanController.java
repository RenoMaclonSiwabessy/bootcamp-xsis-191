package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.HewanModel;
import com.xsis.demo.repository.HewanRepo;

@Controller
public class HewanController {
	//Membuat auto instance dari repository
	@Autowired
	private HewanRepo repo;
	
	//Request yang ada di url localhost:port/hewan/index
	@RequestMapping("/hewan/index")
	public String index(Model model) {
		//Membuat object list biodata, kemudian diisi dari object repo dengan method findAll
		List<HewanModel> dataHewan = repo.findAll();
		
		//Mengirim variable listdata, valuenya diisi dari object dataHewan
		model.addAttribute("listData", dataHewan);
		
		//Menampilakn view /src/main/resource/templates
		return "hewan/index";
	}
	
	@RequestMapping("/hewan/add")
	public String add() {
		return "hewan/add";
	}
	
	@RequestMapping(value="hewan/edit/{id}")
	public String edit(Model model,@PathVariable(name="id")Integer id) {
		HewanModel item = repo.findById(id).orElse(null);
		return "hewan/edit";
	}
	
	@RequestMapping(value="/hewan/save", method = RequestMethod.POST)
	public String save(@ModelAttribute HewanModel item) {
		//Simpan ke database
		repo.save(item);
		//Redirect : akan diteruskan ke halman index
		return "redirect:/hewan/save";
	}
	
	//Delete
	@RequestMapping(value="/hewan/delete/{id}")
	public String hapus(@PathVariable(name="id")Integer id) {
		//Mengambil data dari database dengan parameter id
		HewanModel item = repo.findById(id).orElse(null);
		//Hapus dari database
		if(item != null) {
			repo.delete(item);
		}
		
		return "redirect:/hewan/index";
	}
}
