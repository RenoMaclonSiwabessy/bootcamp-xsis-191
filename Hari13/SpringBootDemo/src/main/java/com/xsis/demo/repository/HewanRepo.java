package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.HewanModel;

@Repository
public interface HewanRepo extends JpaRepository<HewanModel, Integer> {

}
