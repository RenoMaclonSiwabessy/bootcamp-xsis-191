package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping(value="/home")
	public String index() {
		return "home/index";
	}
	
	@RequestMapping(value="/home/contact")
	public String contact() {
		return "home/contact";
	}
	
	@RequestMapping(value="/home/about")
	public String about() {
		return "home/about";
	}
	
	@RequestMapping(value="/home/profile")
	public String profile() {
		return "home/profile";
	}

}
