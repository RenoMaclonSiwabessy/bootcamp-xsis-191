package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.MobilModel;

@Controller
public class MobilController {
	@RequestMapping("/mobil/index")
	public String index() {
		return "mobil/index";
	}
	
	@RequestMapping("/mobil/add")
	public String add() {
		return "mobil/add";
	}
	
	@RequestMapping(value="/mobil/save", method = RequestMethod.POST)
	public String save(@ModelAttribute MobilModel item, Model model) {
		model.addAttribute("data", item);
		return "mobil/save";
	}

}
