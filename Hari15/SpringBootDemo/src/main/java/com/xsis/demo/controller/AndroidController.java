package com.xsis.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.AndroidModel;

@Controller
public class AndroidController {
	@RequestMapping("/android/index")
	public String index() {
		return "android/index";
	}
	
	@RequestMapping("/android/add")
	public String add() {
		return "android/add";
	}
	
	@RequestMapping(value="/android/save", method = RequestMethod.POST)
	public String save(@ModelAttribute AndroidModel item, Model model) {
		model.addAttribute("android", item);
		return "android/save";
	}
}
