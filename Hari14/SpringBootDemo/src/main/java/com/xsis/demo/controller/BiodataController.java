package com.xsis.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.demo.model.BiodataModel;
import com.xsis.demo.repository.BiodataRepo;

@Controller
public class BiodataController {
	//Membuat auto instance dari repository
	@Autowired
	private BiodataRepo repo;
	
	//Request yang ada di url localhost:port/biodata/index
	@RequestMapping("/biodata/index")
	public String index(Model model) {
		//Membuat object list biodata, kemudian diisi dari object repo dengan method finAll
		List<BiodataModel> data = repo.findAll();
		
		//Mengirim variable listData, valuenya diisi dari object data
		model.addAttribute("listData", data);
		
		//Menampilkan view /src/main/resources/templates
		return "biodata/index";
	}
	
	//Request yang ada di url localhost:port/biodata/add
	@RequestMapping("/biodata/add")
	public String add() {
		//Menampilkan  vies /src/main/resources/templates
		return "biodata/add";
	}
	
	//Request edit data	
	@RequestMapping(value="/biodata/edit/{id}")
	public String edit(Model model, @PathVariable(name="id")Integer id) {
		BiodataModel item	= repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "biodata/edit";
	}
	
	//delete
	@RequestMapping(value = "/biodata/delete/{id}")
	public String hapus(@PathVariable(name="id")Integer id) {
		//Mengambil data dari database dengan parameter id
		BiodataModel item = repo.findById(id).orElse(null);
		//Remove From database
		if(item != null) {
			repo.delete(item);
		}
		
		return "redirect:/biodata/index";
	}
	
	//Request yang ada di url localhost:port/biodata/save
	//Memiliki method POST
	@RequestMapping(value = "/biodata/save", method = RequestMethod.POST)
	public String save(@ModelAttribute BiodataModel item) {
		//Simpan ke database
		repo.save(item);
		
		//Redirect : akan diteruskan ke halaman index
		return "redirect:/biodata/index";
	}
}
