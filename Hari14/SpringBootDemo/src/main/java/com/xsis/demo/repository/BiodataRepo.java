package com.xsis.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.demo.model.BiodataModel;

@Repository
public interface BiodataRepo extends JpaRepository<BiodataModel, Integer>{

}
