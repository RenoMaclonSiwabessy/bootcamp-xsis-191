package com.xsis.api.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.JurusanModel;
import com.xsis.api.repository.JurusanRepo;

@Controller
public class JurusanController {
	@Autowired
	private JurusanRepo repo;
	
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/jurusan/", method=RequestMethod.GET)
	public ResponseEntity<List<JurusanModel>> list(){
		ResponseEntity<List<JurusanModel>> result = null;
		try {
			List<JurusanModel> items = repo.findAll();
			result = new ResponseEntity<List<JurusanModel>>(items, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<List<JurusanModel>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/jurusan/", method=RequestMethod.POST)
	public ResponseEntity<JurusanModel> save(@RequestBody JurusanModel item){
		ResponseEntity<JurusanModel> result = null;
		try {
			repo.save(item);
			result = new ResponseEntity<JurusanModel>(item, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<JurusanModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
