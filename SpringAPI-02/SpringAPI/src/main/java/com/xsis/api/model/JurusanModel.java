package com.xsis.api.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="jurusan")
public class JurusanModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE,generator="jurusan_seq")
	@TableGenerator(name="jurusan_seq",table="tbl_sequence", pkColumnName="seq_id", valueColumnName="seq_value", initialValue=0, allocationSize=1)
	private Long id;
	
	@Column(name="kd_jurusan", nullable=false, length=10)
	private String kode;
	
	@Column(name="nm_jurusan", nullable=false, length=150)
	private String nama;
	
	@Column(name="fakultas_id", nullable=false, updatable=false,insertable=false)
	private Long fakultasId;

	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="fakultas_id", foreignKey=@ForeignKey(name="fk_fakultas"))
	private FakultasModel fakultas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Long getFakultasId() {
		return fakultasId;
	}

	public void setFakultasId(Long fakultasId) {
		this.fakultasId = fakultasId;
	}

	public FakultasModel getFakultas() {
		return fakultas;
	}

	public void setFakultas(FakultasModel fakultas) {
		this.fakultas = fakultas;
	}
	
}
