package com.xsis.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.api.model.FakultasModel;

@Repository
public interface FakultasRepo extends JpaRepository<FakultasModel, Long>{

}
