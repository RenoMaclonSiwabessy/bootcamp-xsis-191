package com.xsis.api.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.api.model.FakultasModel;
import com.xsis.api.repository.FakultasRepo;

@Controller
public class FakultasController {
	@Autowired
	private FakultasRepo repo;
	
	private Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/api/fakultas/", method=RequestMethod.GET)
	public ResponseEntity<List<FakultasModel>> list(){
		ResponseEntity<List<FakultasModel>> result = null;
		try {
			List<FakultasModel> items = repo.findAll();
			result = new ResponseEntity<List<FakultasModel>>(items, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<List<FakultasModel>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@RequestMapping(value="/api/fakultas/", method=RequestMethod.POST)
	public ResponseEntity<FakultasModel> save(@RequestBody FakultasModel item){
		ResponseEntity<FakultasModel> result = null;
		try {
			repo.save(item);
			result = new ResponseEntity<FakultasModel>(item, HttpStatus.OK);
		} catch (Exception e) {
			log.debug(e.getMessage(),e);
			result = new ResponseEntity<FakultasModel>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
}
