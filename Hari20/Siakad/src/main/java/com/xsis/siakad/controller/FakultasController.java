package com.xsis.siakad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.siakad.model.FakultasModel;
import com.xsis.siakad.repository.FakultasRepo;

@Controller
public class FakultasController {
	//Membuat auto instance dari repository
	@Autowired
	private FakultasRepo repo;
	
	//Request yang ada di url localhost:port/fakultas/index
	@RequestMapping(value = "fakultas/index", method = RequestMethod.GET)
	public String index(Model model) {
		//Membuat object list fakultas, kemudiann diisi dari object repo dengan method findAll
		List<FakultasModel> data = repo.findAll();
		//Mengirim variable list data, valuenya diisi dari object data
		model.addAttribute("listData", data);
		//Menampilkan view
		return "fakultas/index";
	}
	
	@RequestMapping("fakultas/add")
	public String add() {
		return "fakultas/add";
	}
	
	//Request Save
	//Memiliki method post
	@RequestMapping(value = "/fakultas/save", method = RequestMethod.POST)
	public String save(@ModelAttribute FakultasModel item) {
		//Simpan ke database
		repo.save(item);
		//Redirect : akan diteruskan ke halaman index
		return "redirect:/fakultas/index";
		
	}
	
	//Request Edit
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		FakultasModel item = repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "fakultas/edit";
	}
	
	//Request delete data
	@RequestMapping(value = "/fakultas/edit/{id}")
	public String hapus(@PathVariable(name = "id") Integer id) {
		//Mengambil data dari database dengan parameter id
		FakultasModel item = repo.findById(id).orElse(null);
		//Remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/fakultas/index";
	}
	
	@RequestMapping(value = "/fakultas/list", method = RequestMethod.GET)
	public String tampilList(Model model) {
		List<FakultasModel> item = repo.findAll();
		model.addAttribute("listData", item);
		return "fakultas/list";
	}
}
