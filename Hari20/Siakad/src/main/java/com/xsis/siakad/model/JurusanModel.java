package com.xsis.siakad.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table(name = "jurusan")
@Data
public class JurusanModel {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "jurusan_seq")
	@TableGenerator(name = "jurusan_seq", table = "tbl_sequence", pkColumnName = "seq_id",
	valueColumnName = "seq_value", initialValue = 0, allocationSize = 1)
	private int id;
	
	@Column(name = "kd_jurusan", nullable = false, length = 10)
	private String kode;
	
	@Column(name = "nm_jurusan", nullable = false, length = 100)
	private String nama;
	
	@Column(name = "fakultas_id", nullable = false)
	private int fakultasId;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "fakultas_id", foreignKey = @ForeignKey(name = "fk_fakultas"), updatable = false, insertable = false)
	private FakultasModel fakultas;
}
