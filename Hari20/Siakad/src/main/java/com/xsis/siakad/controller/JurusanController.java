package com.xsis.siakad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xsis.siakad.model.JurusanModel;
import com.xsis.siakad.repository.JurusanRepo;

@Controller
public class JurusanController {
	//Membuat auto instance dari repository
	@Autowired
	private JurusanRepo repo;
	
	@RequestMapping("jurusan/index")
	public String index(Model model) {
		//Membuat object list employee, kemudian diisi dari object repo dengan method findAll
		List<JurusanModel> data = repo.findAll();
		//Mengirim variable list dara, valuenya diisi dari object data
		model.addAttribute("listData", data);
		return "jurusan/index";
	}
	
	//Request map ke halaman add
	@RequestMapping("/jurusan/add")
	public String add() {
		return "jurusan/add";
	}
	
	//Request mapping save
	@RequestMapping(value = "/jurusan/save", method = RequestMethod.POST)
	public String save(@ModelAttribute JurusanModel item) {
		//Simpan ke database
		repo.save(item);
		return "redirect:/jurusan/index;";
	}
	//Request Edit data
	public String edit(Model model, @PathVariable(name = "id") Integer id) {
		JurusanModel item = repo.findById(id).orElse(null);
		model.addAttribute("data", item);
		return "jurusan/edit";
	}
	
	//Request Delete data
	public String hapus(@PathVariable(name = "id") Integer id) {
		//Mengambil data dari database dengan parameter id
		JurusanModel item = repo.findById(id).orElse(null);
		//Remove from database
		if (item != null) {
			repo.delete(item);
		}
		return "redirect:/jurusan/index";
	}
}
